package com.kit1414.eShopCatalog.client.integration;

import com.kit1414.eShopCatalog.client.rest.RestClient;

/**
 * Created by kit_1414 on 12-Feb-17.
 */
public class BaseRestTest {
    protected static final String BASE_URL = "http://localhost:8080/e-shop-catalog";

    protected RestClient restClient;

    public void setUp() {
        restClient = new RestClient(BASE_URL);
    }


}
