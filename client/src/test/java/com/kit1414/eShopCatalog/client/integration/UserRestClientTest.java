package com.kit1414.eShopCatalog.client.integration;


import com.kit1414.eShopCatalog.client.rest.RestResponse;
import com.kit1414.eShopCatalog.core.beans.RoleBean;
import com.kit1414.eShopCatalog.core.beans.UserBean;
import com.kit1414.eShopCatalog.core.enums.RoleEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserRestClientTest extends BaseRestTest {
    private static final Logger logger = LoggerFactory.getLogger(UserRestClientTest.class);

    @BeforeMethod
    public void setUp() {
        super.setUp();
    }

    @Test
    public void getAllUsersTest() throws Exception {
        RestResponse<List<UserBean>> response = restClient.listAllUsers();
        Assert.assertEquals(HttpStatus.OK,response.getStatus());
        logger.debug("status="+ response .getStatus());
        logger.debug("listSize="+ response .getBody().size());
        Assert.assertTrue(response .getBody().size() > 2);
        for (UserBean user : response .getBody()) {
            logger.debug("user=" + user);
        }

    }

    @Test
    public void getOneUserTest() throws Exception {
        RestResponse<UserBean> response = restClient.getUser(1);
        Assert.assertEquals(1,response.getBody().getId().longValue());
        Assert.assertEquals(HttpStatus.OK,response.getStatus());
        logger.debug("response = "+ response);
    }
    @Test
    public void crudUserTest() throws Exception {

        int oldSize=0;
        {
            RestResponse<List<UserBean>> response = restClient.listAllUsers();
            oldSize = response.getBody().size();
        }
        UserBean newUser = createUser(439);

        long id = 0;
        {
            RestResponse<Long> responseLong = restClient.createUser(newUser);
            logger.debug("responseLong = "+ responseLong);

            Assert.assertEquals(HttpStatus.OK,responseLong.getStatus());
            id = responseLong.getBody();
            newUser.setId(id);
        }

        UserBean checkUser = restClient.getUser(id).getBody();
        checkNotNull(checkUser);
        checkSame(checkUser, newUser);

        int newSize=0;
        {
            RestResponse<List<UserBean>> response = restClient.listAllUsers();
            newSize = response.getBody().size();
            Assert.assertEquals(oldSize+1, newSize);
        }


        newUser.setLastName("zzzz");
        {
            RestResponse<UserBean> responseUpdate = restClient.updateUser(newUser);
            logger.debug("responseUpdate = "+ responseUpdate);

            Assert.assertEquals(HttpStatus.OK,responseUpdate.getStatus());
            checkUser = responseUpdate.getBody();
        }
        checkNotNull(checkUser);
        checkSame(checkUser, newUser);
        {
            RestResponse<Boolean> responseDelete = restClient.deleteUser(id);
            Assert.assertEquals(HttpStatus.OK, responseDelete.getStatus());
            Boolean deleted = responseDelete.getBody();
            Assert.assertEquals(true, deleted.booleanValue());
        }

        {
            RestResponse<List<UserBean>> response = restClient.listAllUsers();
            newSize = response.getBody().size();
            Assert.assertEquals(oldSize, newSize);
        }

    }
    protected UserBean createUser(long t) {
        UserBean newUser = new UserBean();
        newUser.setLogin("newUser_login-"+t);
        newUser.setLastName("newUser_lastName_" + t);
        newUser.setFirstName("newUser_firstName_" + t);
        newUser.setDescription("newUser_description_" + t);
        newUser.setEmail("newUser.email." + t + "@gmail.com");
        newUser.setPassMd5("md5_for_new_user-" + t);
        newUser.setRoles(new ArrayList<>());
        newUser.getRoles().add(new RoleBean(RoleEnum.CUSTOMER.getId()));
        newUser.getRoles().add(new RoleBean(RoleEnum.GUEST.getId()));

        return newUser;
    }
    protected void checkNotNull(UserBean u1) {
        Assert.assertNotNull(u1.getDescription());
        Assert.assertNotNull(u1.getLastName());
        Assert.assertNotNull(u1.getFirstName());
        Assert.assertNotNull(u1.getLogin());
        Assert.assertNotNull(u1.getId());
        Assert.assertNotNull(u1.getEmail());
        Assert.assertNotNull(u1.getPassMd5());
    }
    protected void checkSame(UserBean u1, UserBean u2) {
        checkNotNull(u1);
        Assert.assertEquals(u1.getDescription(),u2.getDescription());
        Assert.assertEquals(u1.getLastName(),u2.getLastName());
        Assert.assertEquals(u1.getFirstName(),u2.getFirstName());
        Assert.assertEquals(u1.getLogin().toLowerCase(),u2.getLogin().toLowerCase());
        Assert.assertEquals(u1.getId(),u2.getId());
        Assert.assertEquals(u1.getEmail().toLowerCase(),u2.getEmail().toLowerCase());
        Assert.assertEquals(u1.getPassMd5(),u2.getPassMd5());
        Set<RoleBean> roles1 = new HashSet<>(u1.getRoles());
        Set<RoleBean> roles2 = new HashSet<>(u2.getRoles());
        roles1.removeAll(roles2);
        Assert.assertEquals(0,roles1.size());
    }

}
