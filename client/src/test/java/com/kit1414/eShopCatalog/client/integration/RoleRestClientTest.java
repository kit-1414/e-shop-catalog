package com.kit1414.eShopCatalog.client.integration;

import com.kit1414.eShopCatalog.client.rest.RestResponse;
import com.kit1414.eShopCatalog.core.beans.RoleBean;
import com.kit1414.eShopCatalog.core.beans.UserBean;
import com.kit1414.eShopCatalog.core.enums.RoleEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by kit_1414 on 12-Feb-17.
 */
public class RoleRestClientTest extends  BaseRestTest {
    private static final Logger logger = LoggerFactory.getLogger(RoleRestClientTest.class);

    @BeforeMethod
    public void setUp() {
        super.setUp();
    }

    @Test
    public void getAllRolesTest() throws Exception {
        RestResponse<List<RoleBean>> response = restClient.listAllRoles();

        logger.debug("status="+ response .getStatus());
        logger.debug("listSize="+ response .getBody().size());

        Assert.assertEquals(HttpStatus.OK,response.getStatus());
        Assert.assertEquals(response .getBody().size(), RoleEnum.values().length);

        for (RoleBean role: response .getBody()) {
            logger.debug("role=" + role);
        }

    }

}
