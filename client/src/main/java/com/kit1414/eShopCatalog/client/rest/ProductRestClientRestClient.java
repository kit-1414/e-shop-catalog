package com.kit1414.eShopCatalog.client.rest;

import com.kit1414.eShopCatalog.core.beans.ProductBean;

/**
 * Created by kit_1414 on 12-Feb-17.
 */
public class ProductRestClientRestClient extends BaseRestClient<ProductBean> {
    public ProductRestClientRestClient(String baseUrl) {
        super(baseUrl, "product", "products");
    }
}
