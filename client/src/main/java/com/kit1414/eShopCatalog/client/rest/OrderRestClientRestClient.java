package com.kit1414.eShopCatalog.client.rest;

import com.kit1414.eShopCatalog.core.beans.OrderBean;

/**
 * Created by kit_1414 on 12-Feb-17.
 */
public class OrderRestClientRestClient extends BaseRestClient<OrderBean> {
    public OrderRestClientRestClient(String baseUrl) {
        super(baseUrl, "order", "orders");
    }
}
