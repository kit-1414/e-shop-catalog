package com.kit1414.eShopCatalog.client.rest;

import com.google.common.collect.Sets;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Set;

/**
 * Created by kit_1414 on 12-Feb-17.
 */
public class  RestResponse<T> {
    private T body;
    private HttpStatus status;

    private static final Set<HttpStatus> OK_STATUSES = Sets.newHashSet(HttpStatus.OK,HttpStatus.CREATED, HttpStatus.NO_CONTENT);

    public RestResponse() {
    }

    public RestResponse(T body, HttpStatus status) {
        this.body = body;
        this.status = status;
    }
    public RestResponse(ResponseEntity<T> responseEntity) {
        this.body = responseEntity.getBody();
        this.status = responseEntity.getStatusCode();
    }

    public boolean isOk() {
        return OK_STATUSES.contains(getStatus());
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "RestResponse{" +
                ", status=" + status +
                " body=" + body +
                '}';
    }
}
