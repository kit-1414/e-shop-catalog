package com.kit1414.eShopCatalog.client.rest;

import com.kit1414.eShopCatalog.core.beans.RoleBean;

/**
 * Created by kit_1414 on 12-Feb-17.
 */
public class RoleRestClientRestClient extends BaseRestClient<RoleBean> {
    public RoleRestClientRestClient(String baseUrl) {
        super(baseUrl, "role", "roles");
    }
}
