package com.kit1414.eShopCatalog.client.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kit1414.eShopCatalog.core.beans.BaseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kit_1414 on 12-Feb-17.
 */
public class BaseRestClient<T extends BaseBean> {

    private static final Logger logger = LoggerFactory.getLogger(BaseRestClient.class);

    private final Class<T> beanClass;

    private final String baseUrl;
    private final String beanPath;
    private final String beansPath;


    public BaseRestClient(String baseUrl, String onePath, String manyPath) {
        Type[] types = ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments();
        this.beanClass = (Class<T>) types[0];
        this.baseUrl = baseUrl;
        this.beanPath = onePath;
        this.beansPath = manyPath;
    }

    private RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }
    private String restPath(String path) {
        String fullPath= baseUrl + path;
        logger.debug("restPath() fullPath=" + fullPath);
        return fullPath;
    }
    private String restOnePath() {
        return restPath("/" + this.beanPath);
    }
    private String restOnePath(long id) {
        return restPath("/"  + this.beanPath + "/" + id);
    }
    private String restManyPath() {
        return restPath("/" + this.beansPath);
    }

    /* GET */
    @SuppressWarnings("unchecked")
    public RestResponse<List<T>> listAllBeans(){
        logger.debug("listAllBeans()");
        ResponseEntity<List> response =  getRestTemplate().getForEntity(restManyPath(), List.class);

        logger.debug("status=" + response.getStatusCode());

        RestResponse<List<T>> result = new RestResponse<>();
        result.setBody(new ArrayList<>());
        result.setStatus(response.getStatusCode());
        if ( response.getStatusCode() == HttpStatus.OK) {
            List list = response.getBody();
            if (list != null) {
                for (Object map : list) {
                    ObjectMapper mapper = new ObjectMapper();
                    T bean = mapper.convertValue(map, beanClass);
                    result.getBody().add(bean);
                }
            }
        }
        return result;
    }

    /* GET */
    public RestResponse<T> getBean(long id){
        logger.debug("getBean() id=" + id);
        ResponseEntity<T> response =  getRestTemplate().getForEntity(restOnePath(id), beanClass);
        RestResponse<T> result = new RestResponse<>(response);
        logger.debug("result=" + result);
        return result;
    }

    /* POST */
    public RestResponse<Long> createBean(T bean) {
        logger.debug("createBean() bean=" + bean);
        ResponseEntity<Long> response =  getRestTemplate().postForEntity(restOnePath(), bean, Long.class);
        return new RestResponse<>(response);
    }

    /* PUT */
    public RestResponse<T> updateBean(T bean) {
        logger.debug("updateBean() bean=" + bean);
        HttpEntity<T> entity = new HttpEntity<>(bean);
        long id  = bean.getId();
        ResponseEntity<T> response =  getRestTemplate().exchange(restOnePath(id), HttpMethod.PUT,entity, this.beanClass);
        return new RestResponse<>(response);
    }

    /* DELETE */
    public RestResponse<Boolean> deleteBean(long id) {
        logger.debug("deleteBean() id=" + id);
        HttpEntity<Long> entity = new HttpEntity<Long>(id);
        ResponseEntity<Boolean> response =  getRestTemplate().exchange(restOnePath(id),HttpMethod.DELETE,entity, Boolean.class);
        return new RestResponse<>(response);
    }
}
