package com.kit1414.eShopCatalog.client.rest;

import com.kit1414.eShopCatalog.core.beans.UserBean;

/**
 * Created by kit_1414 on 12-Feb-17.
 */
public class UserRestClientRestClient extends BaseRestClient<UserBean> {
    public UserRestClientRestClient(String baseUrl) {
        super(baseUrl, "user", "users");
    }
}
