package com.kit1414.eShopCatalog.client.rest;

import com.kit1414.eShopCatalog.core.beans.OrderBean;
import com.kit1414.eShopCatalog.core.beans.ProductBean;
import com.kit1414.eShopCatalog.core.beans.RoleBean;
import com.kit1414.eShopCatalog.core.beans.UserBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.List;

public class RestClient {

    private static final Logger logger = LoggerFactory.getLogger(RestClient.class);


    private final UserRestClientRestClient userClient;
    private final RoleRestClientRestClient roleClient;
    private final ProductRestClientRestClient productClient;
    private final OrderRestClientRestClient orderClient;

    public RestClient(String baseUrl) {
        this.roleClient = new RoleRestClientRestClient(baseUrl);
        this.userClient = new UserRestClientRestClient(baseUrl);
        this.productClient = new ProductRestClientRestClient(baseUrl);
        this.orderClient = new OrderRestClientRestClient(baseUrl);
    }
    //  ---  Roles ---
    public RestResponse<List<RoleBean>> listAllRoles(){
        logger.debug("listAllRoles()");
        return roleClient.listAllBeans();
    }

    //  ---  Users ---
    public RestResponse<List<UserBean>> listAllUsers(){
        logger.debug("listAllUsers()");
        return userClient.listAllBeans();
    }

    public RestResponse<UserBean> getUser(long id){
        logger.debug("getUser() id=" + id);
        return userClient.getBean(id);
    }

    public RestResponse<Long> createUser(UserBean user) {
        logger.debug("createUser() user=" + user);
        return userClient.createBean(user);
    }

    public RestResponse<UserBean> updateUser(UserBean user) {
        logger.debug("updateUser() user=" + user);
        return userClient.updateBean(user);
    }

    public RestResponse<Boolean> deleteUser(long id) {
        logger.debug("deleteUser() id=" + id);
        return userClient.deleteBean(id);
    }
    //  ---  Products ---
    public RestResponse<List<ProductBean>> listAllProducts(){
        logger.debug("listAllProducts()");
        return productClient.listAllBeans();
    }

    public RestResponse<ProductBean> getProduct(long id){
        logger.debug("getProducts() id=" + id);
        return productClient.getBean(id);
    }

    public RestResponse<Long> createProduct(ProductBean product) {
        logger.debug("createProduct() product=" + product);
        return productClient.createBean(product);
    }

    public RestResponse<ProductBean > updateProduct(ProductBean product) {
        logger.debug("updateProduct() product=" + product);
        return productClient.updateBean(product);
    }

    public RestResponse<Boolean> deleteProduct(long id) {
        logger.debug("deleteProduct() id=" + id);
        return productClient.deleteBean(id);
    }

    //  ---  Orders ---
    public RestResponse<List<OrderBean>> listAllOrders(){
        logger.debug("listAllOrders()");
        return orderClient.listAllBeans();
    }

    public RestResponse<OrderBean> getOrder(long id){
        logger.debug("getOrder() id=" + id);
        return orderClient.getBean(id);
    }

    public RestResponse<Long> createOrder(OrderBean order) {
        logger.debug("createOrder() order=" + order);
        return orderClient.createBean(order);
    }

    public RestResponse<OrderBean> updateOrder(OrderBean order) {
        logger.debug("updateOrder() order=" + order);
        return orderClient.updateBean(order);
    }

    public RestResponse<Boolean> deleteOrder(long id) {
        logger.debug("deleteOrder() id=" + id);
        return orderClient.deleteBean(id);
    }
}