app.controller('UserBaseCtrl',['$scope',  '$controller', '$location',
   function ($scope, $controller, $location ) {
	   angular.extend(this, $controller('BaseController', { $scope: $scope }));

           $scope.gotoUsersList = function() {
		$location.path('/userList');
	   }

           $scope.doCancel = function () {$scope.gotoUsersList();}
}]);
        // var user = SecureService.getCurrentUser();
app.controller('UserListCtrl',['$scope', '$controller', '$location', '$route', 'DataService',
   function ($scope, $controller, $location, $route,  DataService) {

    angular.extend(this, $controller('UserBaseCtrl', { $scope: $scope }));
        // var user = SecureService.getCurrentUser();
        // alert('user='+user);

        DataService.getUsers().then(
		function(users) {
			$scope.users = users
		}, $scope.addError
	);

        $scope.editUser = function (user) {
            console.log(user);
            $location.path('/userEdit/'+user.id);
        }

        $scope.deleteUser = function (user) {
	    $scope.clearErrors();
	    DataService.deleteUser(user).then(
                function() {
                        console.log('user removed ok:' + $scope.userToString(user));
			$route.reload();
		},
		$scope.addError)
        };

}]);

app.controller('UserCreateCtrl', [ '$scope', '$controller','$location',  'DataService', 
	function ($scope, $controller, $location, DataService) {

    angular.extend(this, $controller('UserBaseCtrl', { $scope: $scope }));

    DataService.getRoles().then(function(roles) {$scope.roles = roles}, $scope.addError);

    $scope.clearErrors();
    $scope.createUser = function () {
        console.log("create user : " + $scope.user);
	$scope.clearErrors();
        if ($scope.user.password1 != $scope.user.password2) {
        	$scope.addError("Error : passwd is differ");
	} else {
		$scope.user.passMd5 = "";
                if ($scope.user.password1) {
			$scope.user.passMd5 = CryptoJS.MD5($scope.user.password1)+"";
		}
		delete $scope.user.password1;
		delete $scope.user.password2;
    		DataService.createUser($scope.user).then($scope.gotoUsersList,$scope.addError);
	}
    }

}]);
app.controller('UserEditCtrl',['$scope', '$controller', '$location', '$routeParams', 'DataService',
	 function ($scope, $controller, $location, $routeParams, DataService) {

//    angular.extend(this, $controller('UserBaseCtrl', { $scope: $scope }));
    angular.extend(this, $controller('UserBaseCtrl', { $scope: $scope }));

    $scope.clearErrors();
    DataService.getUser($routeParams.id).then(function(user) {$scope.user = user});
    DataService.getRoles().then(function(roles) {$scope.roles = roles});

    $scope.updateUser = function () {
        console.log("update user : "  + $scope.userToString($scope.user));
	$scope.clearErrors();
        DataService.updateUser($scope.user).then($scope.gotoUsersList,$scope.addError);
    }

    $scope.updatePassword = function() {
        $scope.clearErrors();
        console.log("update password : "  + $scope.userToString($scope.user));
        if ($scope.user_password1 != $scope.user_password2) {
        	console.log("update password : Error : passwd is differ" );
        	$scope.addError("Error : passwd is differ");
	} else {
		var newPassMd5 = "";
                if ($scope.user_password1) {
			newPassMd5 = CryptoJS.MD5($scope.user_password1)+"";
		}
		console.log("new passwd=" +$scope.user_password1+ ", hash=" + newPassMd5);
		// load user again from server, replace pass and send to update
    		DataService.getUser($routeParams.id).then(
                        function(userToUpdate) {
			   console.log("new passwd: user reloaded, old pass = " + userToUpdate.passMd5);
   			   userToUpdate.passMd5 = newPassMd5;
			   console.log("new passwd: user reloaded, new pass = " + userToUpdate.passMd5);
			   return DataService.updateUser(userToUpdate);
                        },$scope.addError)
               .then($scope.gotoUsersList,$scope.addError);
	}
    }
}]);
