app.factory('DataService',['$rootScope', '$http','$q', 'ServerService' ,function($rootScope, $http, $q, ServerService)  {
  var getAbout = function() {
	var deferred = $q.defer();
	ServerService.doGet('/about', function(data) { deferred.resolve(data);}, 
		function(response) {deferred.reject ("getAbout(http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };

 // --- roles
  var getRoles = function() {
	var deferred = $q.defer();
	ServerService.doGet('/roles', function(roles) { deferred.resolve(roles);}, 
		function(response) {deferred.reject ("getRoles(http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };

  var getRole = function(id) {
	var deferred = $q.defer();
	ServerService.doGet('/role/' + id, function(role) { deferred.resolve(role);},
		function(response) {deferred.reject ("getRole("+id+", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };

 // --- users
  var getUsers = function() {
	var deferred = $q.defer();
	ServerService.doGet('/users',function(users) { deferred.resolve(users);},
		function(response) {deferred.reject ("getUsers(http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };

  var getUser = function(id) {
	var deferred = $q.defer();
	ServerService.doGet('/user/' + id, function(user) {deferred.resolve(user);},
		function(response) {deferred.reject ("getUser("+id+", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };


  var createUser = function (user) {
	var deferred = $q.defer();
        ServerService.doPost('/user', user,
		 function (user) {deferred.resolve(user)},
 		 function (response) {deferred.reject ("updateUser(" + userToString(user) +", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };
  var updateUser = function (user) {
	var deferred = $q.defer();
        ServerService.doPut('/user/'+user.id, user,
		 function (user) {deferred.resolve(user)},
 		 function (response) {deferred.reject ("updateUser(" + userToString(user) +", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };

  var deleteUser = function (user) {
	var deferred = $q.defer();
        ServerService.doDelete('/user/'+user.id, /* user, */
		 function (user) {deferred.resolve("OK")},
 		 function (response) {deferred.reject ("deleteUser(" + userToString(user) +", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };
  // --- product
  var getProducts = function() {
	var deferred = $q.defer();
	ServerService.doGet('/products',function(products) { deferred.resolve(products);},
		function(response) {deferred.reject ("getProducts(http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };

  var getProduct = function(id) {
	var deferred = $q.defer();
	ServerService.doGet('/product/' + id, function(product) {deferred.resolve(product);},
		function(response) {deferred.reject ("getProduct("+id+", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };


  var createProduct = function (product) {
	var deferred = $q.defer();
        ServerService.doPost('/product', product,
		 function (product) {deferred.resolve(product)},
 		 function (response) {deferred.reject ("updateProduct(" + productToString(product) +", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };
  var updateProduct = function (product) {
	var deferred = $q.defer();
        ServerService.doPut('/product/'+product.id, product,
		 function (product) {deferred.resolve(product)},
 		 function (response) {deferred.reject ("updateProduct(" + productToString(product) +", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };

  var deleteProduct = function (product) {
	var deferred = $q.defer();
        ServerService.doDelete('/product/'+product.id, /* product, */
		 function (product) {deferred.resolve("OK")},
 		 function (response) {deferred.reject ("deleteProduct(" + productToString(product) +", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };
  // --- orders
  var getOrders = function() {
	var deferred = $q.defer();
	ServerService.doGet('/orders',function(orders) { deferred.resolve(orders);},
		function(response) {deferred.reject ("getOrders(http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };

  var getOrder = function(id) {
	var deferred = $q.defer();
	ServerService.doGet('/order/' + id, function(order) {deferred.resolve(order);},
		function(response) {deferred.reject ("getOrder("+id+", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };


  var createOrder = function (order) {
	var deferred = $q.defer();
        ServerService.doPost('/order', order,
		 function (order) {deferred.resolve(order)},
 		 function (response) {deferred.reject ("updateOrder(" + orderToString(order) +", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };
  var updateOrder = function (order) {
	var deferred = $q.defer();
        ServerService.doPut('/order/'+order.id, order,
		 function (order) {deferred.resolve(order)},
 		 function (response) {deferred.reject ("updateOrder(" + orderToString(order) +", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };

  var deleteOrder = function (order) {
	var deferred = $q.defer();
        ServerService.doDelete('/order/'+order.id, /* order, */
		 function (order) {deferred.resolve("OK")},
 		 function (response) {deferred.reject ("deleteOrder(" + orderToString(order) +", http) failed, status=" + response.status);}
	);
	return deferred.promise;
  };

  // -- Service
  var userToString = function (user) {
        if (!user) return null;
	return "User {login="+user.login+' , id='+ user.id+'}';
  }
  var productToString = function (product) {
        if (!product) return null;
	return "Product {name="+product.productName+' , id='+ product.id+ ', code='+product.productCode + '}';
  }
  var orderToString = function (order) {
        if (!order) return null;
	return "Order {subject=" + order.subject +' , id='+ order.id + '}';
  }

  
  return {
     getAbout  : getAbout,

     getRoles  : getRoles,
     getRole   : getRole,

     getUsers  : getUsers,
     getUser   : getUser,
     createUser: createUser,
     updateUser: updateUser,
     deleteUser: deleteUser,

     getProducts  : getProducts,
     getProduct   : getProduct,
     createProduct: createProduct,
     updateProduct: updateProduct,
     deleteProduct: deleteProduct,

     getOrders  : getOrders,
     getOrder   : getOrder,
     createOrder: createOrder,
     updateOrder: updateOrder,
     deleteOrder: deleteOrder
  }

}]);
