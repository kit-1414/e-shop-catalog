app.controller('BaseController',['$scope', '$location','SecureService',
   function ($scope, $location, SecureService) {

        $scope.logout = function () {
		SecureService.logout();
		$location.path('/login');
        }
	$scope.addAlert = function(type, message) {
    		$scope.alerts.push({type: type, msg: message});
		$scope.hasAlerts = true;
	};

        $scope.addError = function(error) {
	        $scope.addAlert('danger', error);
	}
        $scope.clearErrors = function() {
		$scope.alerts = [];
		$scope.hasAlerts = false;
	}

	$scope.closeAlert = function (index) {
	    $scope.alerts.splice(index, 1);
            $scope.hasAlerts = $scope.alerts.length() == 0;
	}

	$scope.formErrorMessage = function (pageErrorTitle, response) {
	   return pageErrorTitle + ", status=" + response.status;
	}

	$scope.clearErrors();

        $scope.userToString = function (user) {
           if (!user) return null;
	   return "User {login="+user.login+' , id='+ user.id+'}';
        }

        $scope.productToString = function (product) {
           if (!product) return null;
	   return "Position {name="+product.productName +', code='+product.productCode + ' , id='+ product.id+'}';
        }

	$scope.orderToString = function (order) {
            if (!order) return null;
     	    return "Order {subject=" + order.subject +' , id='+ order.id + '}';
        }

}]);
