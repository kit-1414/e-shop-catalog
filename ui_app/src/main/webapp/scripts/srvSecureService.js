app.factory('SecureService',['$rootScope', '$http','$q', 'ServerService', 
  function($rootScope, $http, $q, ServerService)  {
      var userBean = null;

      var securedControllers=[
	'UserListCtrl','UserCreateCtrl','UserEditCtrl',
	'ProductListCtrl','ProductCreateCtrl','ProductEditCtrl',
	'OrderListCtrl','OrderCreateCtrl','OrderEditCtrl'
      ];

      var validRoles=['ADMIN','MANAGER','CUSTOMER','GUEST'];

      var loginNotificationMsg = 'LOGIN_SUCCESS';
      var logoutNotificationMsg = 'LOGOUT_SUCCESS';

      var raiseEventFunction = function(eventName) {
          $rootScope.$broadcast(eventName);
      };
      var isRole = function(index) {
         var expRole = validRoles[index];
         var roles = getCurrentRoles(userBean);
         return roles.indexOf(expRole) != -1;
      }
      var isAdmin = function() {
	return  isRole(0);
      }
      var isManager = function() {
	return  isRole(1);
      }
      var isCustomer = function() {
	return  isRole(2);
      }
      var isGuest = function() {
	return  isRole(3);
      }

      var  getCurrentUser     = function() { 
	return userBean;
      }
      var getCurrentRoles = function () {
         var roles=[];
         var user = getCurrentUser();
         if (user && user.roles.length > 0) {
  	     user.roles.forEach(function(roleBean, i, arr) {
   	     //alert( i + ": " + roleBean + " (������:" + arr + ")" );
             roles.push(roleBean.roleName);
            });
         }
         return roles;
      }
      var checkValidRole = function(userBean) {
         var roles=getCurrentRoles(userBean);
         var intersect = roles.filter(function(n) {
            return validRoles.indexOf(n) != -1
         });	    
        return intersect.length > 0;
      }

      var loginFunction = function(userToLogin, loginSuccessAction, loginFailAction) {
          var deferred = $q.defer();
          var requestBean = {
             login : userToLogin.login,
             md5Pass : CryptoJS.MD5(userToLogin.password) // TODO : md5 conversion need
          }
          console.log("requestBean { login=" + requestBean.login + ", md5Pass=" + requestBean.md5Pass+ " }");

          var loginSuccess = function(data) {             
              userBean = data;
              if (userBean.login && userBean.login.length > 0 ) { 
                 if (checkValidRole(userBean)) {
                    setAuthHeader(userBean.authToken);
		    deferred.resolve(userBean);
                    if (loginSuccessAction) {
                       loginSuccessAction();
                    }
                    raiseEventFunction(loginNotificationMsg);
                 } else {
                    deferred.reject ('user has not access here. He must be registered user');
                 }
              } else {
                deferred.reject ('user not found or wrong password');
              }
          };
          var loginFail = function(response, request,  data) {             
                var errorMessage = 'login fail : status='+ response.status;
       		console.log(errorMessage);
                deferred.reject (errorMessage);
	  }
          ServerService.doPut('/login?login='+requestBean.login+"&md5="+requestBean.md5Pass, null, loginSuccess, loginFail);
 	  return deferred.promise;
      };
      var setAuthHeader = function(token) {
         var header = null;
         if (token && token.length > 0) {             
            header = 'Basic '+ token;
         }
         $http.defaults.headers.common.Authorization = header; 

      }
      var logoutFunction = function() {
          //var deferred = $q.defer();
          console.log('logout started');
          userBean = null;
          setAuthHeader(null);
          raiseEventFunction(logoutNotificationMsg);
          //deffered.resolve('Logout done ok.');          
 	  //return deferred.promise;
          console.log('logout done');
      };
      var checkLoginRedirectFunc = function(nextControllerName) {
         var ctlIndex = securedControllers.indexOf(nextControllerName);
         var securedController = ctlIndex != -1;
         return userBean == null && securedController;
      }

      var servicePtr = {
          getCurrentUser     : getCurrentUser,
          getCurrentRoles    : getCurrentRoles,
          checkLoginRedirect : checkLoginRedirectFunc,
          login              : loginFunction,
          logout             : logoutFunction,
          isAdmin            : isAdmin,
          isManager          : isManager,
          isCustomer         : isCustomer,
          isGuest            : isGuest,
          getLoginNotificationMsg  : function() {return loginNotificationMsg; },
          getLogoutNotificationMsg : function() {return logoutNotificationMsg; }

      };
      return servicePtr;
}]);
