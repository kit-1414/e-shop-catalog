app.factory('ServerService',['$rootScope', '$http',function($rootScope, $http)  {
  var doHttp = function (url, methodName,data, successFunc, errorFunc, alwaysFunc) {    
     var targetUrl =  serviceUrlStart + url;
     console.log("url="+targetUrl+', method='+methodName);
     var request = { method: methodName, url: targetUrl, data: data};
     $http(request).then(function successCallback(response) {
     	console.log("OK for url="+targetUrl+', method='+methodName);
         if (successFunc) {
             successFunc(response.data, response, request, data);
         }
         if (alwaysFunc) {
            alwaysFunc(response, request, data);
         }
     }, function errorCallback(response) {
         console.log('Error for url='+targetUrl+ ', data=' + data + ', status=' + response.status)
         if (errorFunc) {
             errorFunc(response, request, data);
         }
         if (alwaysFunc) {
            alwaysFunc(response, request, data);
         }
     });
  }
  var doGet = function(url, /*request,*/ sucessFunc, errorFunc,alwaysFunc ) {
      doHttp(url,'GET', null, sucessFunc, errorFunc,alwaysFunc );
  }
  var doPost = function(url, request, sucessFunc, errorFunc,alwaysFunc) {
      doHttp(url,'POST', request, sucessFunc, errorFunc,alwaysFunc );
  }
  var doPut = function(url, request, sucessFunc, errorFunc,alwaysFunc) {
      doHttp(url,'PUT', request, sucessFunc, errorFunc,alwaysFunc );
  }
  var doDelete = function(url, /*request,*/ sucessFunc, errorFunc,alwaysFunc) {
      doHttp(url,'DELETE', null, sucessFunc, errorFunc,alwaysFunc );
  }
  return {
     doGet  : doGet,
     doPost : doPost,
     doPut  : doPut,
     doDelete  : doDelete
  }
}]);
