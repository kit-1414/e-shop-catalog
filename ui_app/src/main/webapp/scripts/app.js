var serviceUrlStart="http://localhost:8080/e-shop-catalog";

var app = angular.module('eShopCatalogApp', [
    'checklist-model',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
]);

app.config(function ($routeProvider, $httpProvider) {
    // $httpProvider.defaults.headers.common['Authorization'] = 'Basic YWRtaW5fbG9naW46YmE0NmUxYmIwOWZjMThiOWFmODlkYTNmNDc2Y2RhZjc=';
    $routeProvider.
    when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
    }).when('/userList', {
        templateUrl: 'views/userList.html',
        controller: 'UserListCtrl'
    }).when('/userCreate', {
        templateUrl: 'views/userCreate.html',
        controller: 'UserCreateCtrl'
    }).when('/userEdit/:id', {
        templateUrl: 'views/userEdit.html',
        controller: 'UserEditCtrl'
    }).when('/productList', {
        templateUrl: 'views/productList.html',
        controller: 'ProductListCtrl'
    }).when('/productCreate', {
        templateUrl: 'views/productCreate.html',
        controller: 'ProductCreateCtrl'
    }).when('/productEdit/:id', {
        templateUrl: 'views/productEdit.html',
        controller: 'ProductEditCtrl'
    }).when('/orderList', {
        templateUrl: 'views/orderList.html',
        controller: 'OrderListCtrl'
    }).when('/orderCreate', {
        templateUrl: 'views/orderCreate.html',
        controller: 'OrderCreateCtrl'
    }).when('/orderEdit/:id', {
        templateUrl: 'views/orderEdit.html',
        controller: 'OrderEditCtrl'
    }).when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
    }).otherwise({
        redirectTo: '/about'
    })
});


app.run(['$rootScope', '$location', 'SecureService', function ($rootScope, $location, SecureService) {
    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        console.log('On start  : templateUrl=' + next.templateUrl +', controller=' + next.controller);

        var nextControllerName = next.controller;
        var user = SecureService.getCurrentUser();
        var needLogin = SecureService.checkLoginRedirect(next.controller);
        if (needLogin) {
            $location.path('/login');
        }
    });
}]);


app.directive('usersList', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/usersListDirective.html',
        replace: true
    };
});

app.directive('errorsBlock', function() {
    return {
        restrict: 'EA',
        templateUrl: 'views/errorBlockDirective.html',
        replace: true
    };
});

app.directive('productFields', function() {
    return {
        restrict: 'EA',
        templateUrl: 'views/productFieldsDirective.html',
        replace: true
    };
});
app.directive('orderFields', function() {
    return {
        restrict: 'EA',
        templateUrl: 'views/orderFieldsDirective.html',
        replace: true
    };
});
