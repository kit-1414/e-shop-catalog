app.controller('MenuCtrl',['$scope','$controller', '$rootScope', 'SecureService', function ($scope, $controller, $rootScope, SecureService) {
    angular.extend(this, $controller('BaseController', { $scope: $scope }));

    $scope.isUserBean  = false;
    $scope.isAdminRole = false;

    var loginMsg = SecureService.getLoginNotificationMsg();
    var logountMsg = SecureService.getLogoutNotificationMsg();

    $rootScope.$on(loginMsg, function(event, data) {
       $scope.isUserBean  = true;
       $scope.isAdminRole = true;
       console.log('msg received='+loginMsg+', event='+event+', data='+data);
    });

    $rootScope.$on(logountMsg, function(event, data) {
       $scope.isUserBean  = false;
       $scope.isAdminRole = false;
       console.log('msg received='+logountMsg+', event='+event+', data='+data);
    });
}]);

