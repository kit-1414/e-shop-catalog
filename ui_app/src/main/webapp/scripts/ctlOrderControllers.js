app.controller('OrderBaseCtrl',['$scope',  '$controller', '$location',
   function ($scope, $controller, $location ) {
	   angular.extend(this, $controller('BaseController', { $scope: $scope }));

           $scope.gotoOrderList = function() {
		$location.path('/orderList');
	   }

           $scope.doCancel = function () {$scope.gotoOrderList();}
}]);

app.controller('OrderListCtrl',['$scope', '$controller', '$location', '$route', 'DataService',
   function ($scope, $controller, $location, $route,  DataService) {

    angular.extend(this, $controller('OrderBaseCtrl', { $scope: $scope }));
    DataService.getOrders().then(
		function(orders) {
			$scope.orders = orders
		}, $scope.addError
	);

        $scope.editOrder = function (order) {
            console.log(order);
            $location.path('/orderEdit/'+order.id);
        }

        $scope.deleteOrder = function (order) {
	    $scope.clearErrors();
	    DataService.deleteOrder(order).then(
                function() {
                        console.log('order removed ok:' + $scope.orderToString(order));
			$route.reload();
		},
		$scope.addError)
        };

}]);

app.controller('OrderEditBaseCtrl',['$scope', '$controller', '$location', '$routeParams', 'DataService',
	 function ($scope, $controller, $location, $routeParams, DataService) {

    angular.extend(this, $controller('OrderBaseCtrl', { $scope: $scope }));

    $scope.clearErrors();

    //todo : load customers && managers by special filter
    DataService.getUsers().then(function(users) {$scope.customers = users; $scope.managers = users;}, $scope.addError);
    DataService.getProducts().then(function(products) {$scope.products = products;}, $scope.addError);

    $scope.createNewPosition = function (product, amount) {
       var newPosition  = {};
       newPosition.id = null;
       newPosition.product   = product;
       newPosition.productAmount   = amount;
       return newPosition;
    }
    $scope.createNewOrder = function () {
       var newOrder  = {};
       newOrder.id = null;
       newOrder.positions   = [];
       newOrder.manager   = null;
       newOrder.customer = null;
       newOrder.subject = null;
       newOrder.description = null;
       newOrder.deliveryAddress = null;
       return newOrder;
    }

    $scope.editPositionPressed = function () {
    	$scope.clearErrors();
        if (!$scope.editIndex && $scope.newPosition ) {
		if($scope.newPosition.product && $scope.newPosition.productAmount) {
                        // looking for same product in order
			var sameProductFounded = false;
                        for (var pIndex in $scope.order.positions) {
                                var position = $scope.order.positions[pIndex];
				if (position.product.id == $scope.newPosition.product.id) {
					position.productAmount = $scope.newPosition.productAmount + position.productAmount;
					sameProductFounded = true;
					break;
				}
			}
			if(!sameProductFounded) {
        			$scope.order.positions.push($scope.newPosition);
			}
		} else {
			$scope.addError("Fill new position fields OK : product  and amount");
		}
	}
	$scope.editPositionValue = "Add New Product";
	$scope.newPosition  =  $scope.createNewPosition(undefined,0);
	$scope.editIndex = false;
    };
    $scope.deletePosition = function (position,index) {
	$scope.order.positions.splice(index,1);
    };
    $scope.editPosition = function (position, index) {
	$scope.editIndex = true;
	$scope.newPosition = position;
    	$scope.editPositionValue = "Done edit #" + (index + 1);
    };
   

    $scope.editPositionPressed();

}]);
app.controller('OrderEditCtrl',['$scope', '$controller', '$location', '$routeParams', 'DataService',
	 function ($scope, $controller, $location, $routeParams, DataService) {

    angular.extend(this, $controller('OrderEditBaseCtrl', { $scope: $scope }));

    DataService.getOrder($routeParams.id).then(function(order) {
	$scope.order = order
	});

    $scope.updateOrder = function () {
        console.log("update order : "  + $scope.orderToString($scope.order));
	$scope.clearErrors();
        DataService.updateOrder($scope.order).then($scope.gotoOrderList,$scope.addError);
    }

}]);
app.controller('OrderCreateCtrl', [ '$scope', '$controller','$location',  'DataService', 
	function ($scope, $controller, $location, DataService) {

    angular.extend(this, $controller('OrderEditBaseCtrl', { $scope: $scope }));

    $scope.order = $scope.createNewOrder();

    $scope.createOrder = function (order) {
        console.log("create Order : " + $scope.orderToString($scope.order));
	$scope.clearErrors();
	DataService.createOrder($scope.order).then($scope.gotoOrderList,$scope.addError);
    }
}]);

