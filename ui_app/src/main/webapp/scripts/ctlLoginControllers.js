app.controller('LoginCtrl',['$scope', '$controller', '$location','SecureService',
   function ($scope, $controller,  $location, SecureService) {
        console.log("LoginCtrl in progress...")

 	angular.extend(this, $controller('BaseController', { $scope: $scope }));

        $scope.login = function () {
	$scope.clearErrors();

        if (!$scope.user || !$scope.user.login) {
           $scope.addError("login filed empty");
	} else {
	SecureService.login($scope.user).then(
		        function() {
        			console.log("login OK")
				$location.path('/about');
			}, $scope.addError);
		}	
	}
}]);
