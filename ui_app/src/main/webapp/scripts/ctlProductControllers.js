app.controller('ProductBaseCtrl',['$scope',  '$controller', '$location',
   function ($scope, $controller, $location ) {
	   angular.extend(this, $controller('BaseController', { $scope: $scope }));

           $scope.gotoProductList = function() {
		$location.path('/productList');
	   }

           $scope.doCancel = function () {$scope.gotoProductist();}
}]);

app.controller('ProductListCtrl',['$scope', '$controller', '$location', '$route', 'DataService',
   function ($scope, $controller, $location, $route,  DataService) {

    angular.extend(this, $controller('ProductBaseCtrl', { $scope: $scope }));
    DataService.getProducts().then(
		function(products) {
			$scope.products = products
		}, $scope.addError
	);

        $scope.editProduct = function (product) {
            console.log(product);
            $location.path('/productEdit/'+product.id);
        }

        $scope.deleteProduct = function (product) {
	    $scope.clearErrors();
	    DataService.deleteProduct(product).then(
                function() {
                        console.log('product removed ok:' + $scope.productToString(product));
			$route.reload();
		},
		$scope.addError)
        };

}]);

app.controller('ProductCreateCtrl', [ '$scope', '$controller','$location',  'DataService', 
	function ($scope, $controller, $location, DataService) {

    angular.extend(this, $controller('ProductBaseCtrl', { $scope: $scope }));

    $scope.clearErrors();
    $scope.createProduct = function (product) {
        console.log("create Product : " + $scope.productToString($scope.product));
	$scope.clearErrors();
	DataService.createProduct($scope.product).then($scope.gotoProductList,$scope.addError);
    }
}]);
app.controller('ProductEditCtrl',['$scope', '$controller', '$location', '$routeParams', 'DataService',
	 function ($scope, $controller, $location, $routeParams, DataService) {

    angular.extend(this, $controller('ProductBaseCtrl', { $scope: $scope }));

    $scope.clearErrors();
    DataService.getProduct($routeParams.id).then(function(product) {$scope.product = product});

    $scope.updateProduct = function () {
        console.log("update product : "  + $scope.productToString($scope.product));
	$scope.clearErrors();
        DataService.updateProduct($scope.product).then($scope.gotoProductList,$scope.addError);
    }
}]);
