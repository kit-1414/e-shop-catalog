app.controller('AboutCtrl',['$scope', '$controller', '$location', 'DataService', 'SecureService',
   function ($scope, $controller,  $location, DataService, SecureService) {
        console.log("About in progress...")

 	angular.extend(this, $controller('BaseController', { $scope: $scope }));

        $scope.aboutData    = "no data received";
        $scope.currentUser  = "no user";
        $scope.currentRoles = "no roles";

        DataService.getAbout().then(
		function(aboutData) {
			$scope.aboutData = aboutData;
		}, $scope.addError
	);
        var user = SecureService.getCurrentUser();
        if (user) {
		$scope.currentUser = user.login;
                var roles = SecureService.getCurrentRoles()
                $scope.currentRoles =  roles.join(", ");
        }
}]);
