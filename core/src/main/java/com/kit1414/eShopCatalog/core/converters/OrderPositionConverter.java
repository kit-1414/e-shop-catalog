package com.kit1414.eShopCatalog.core.converters;

import com.kit1414.eShopCatalog.core.beans.OrderBean;
import com.kit1414.eShopCatalog.core.beans.OrderPositionBean;
import com.kit1414.eShopCatalog.core.entity.OrderEntity;
import com.kit1414.eShopCatalog.core.entity.OrderPositionEntity;
import org.hibernate.Session;

import java.util.Collection;
import java.util.List;

/**
 * Created by kit_1414 on 08-Dec-16.
 */
public class OrderPositionConverter extends BaseConverter<OrderPositionBean, OrderPositionEntity> {
    @Override
    protected OrderPositionEntity copyToEntityInt(OrderPositionBean src, OrderPositionEntity dest) {
        dest.setId(src.getId());
        dest.setProductAmount(src.getProductAmount());
        dest.setProductPrice(src.getProductPrice());
        dest.setProduct(Converters.PRODUCT_CONVERTER.copyToEntity(src.getProduct())); // any case
        return dest;
    }

    @Override
    protected OrderPositionBean copyToBeanInt(OrderPositionEntity src, OrderPositionBean dest) {
        dest.setId(src.getId());
        dest.setProductAmount(src.getProductAmount());
        dest.setProductPrice(src.getProductPrice());
        dest.setProduct(Converters.PRODUCT_CONVERTER.copyToBean(src.getProduct())); // any case
        return dest;
    }
    @Override
    public OrderPositionEntity update(OrderPositionEntity src, OrderPositionEntity dest) {
        dest.setId(src.getId());
        dest.setProductAmount(src.getProductAmount());
        dest.setProductPrice(src.getProductPrice());
        return dest;
    }
    public static List<OrderPositionBean> copyOrderPositions(Collection<OrderPositionEntity> ePositions) {
        return Converters.ORDER_POSITION_CONVERTER.copyToBeansListWithRelations(ePositions,0);
    }
}
