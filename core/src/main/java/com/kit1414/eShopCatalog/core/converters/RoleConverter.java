package com.kit1414.eShopCatalog.core.converters;

import java.util.Collection;
import java.util.List;

import com.kit1414.eShopCatalog.core.beans.RoleBean;
import com.kit1414.eShopCatalog.core.entity.RoleEntity;
import org.hibernate.Session;

/**
 * Created by kit_1414 on 01-Dec-16.
 */
public class RoleConverter extends BaseConverter<RoleBean, RoleEntity> {
    public static final int ROLE_COPY_LEVEL = 0;
    @Override
    protected RoleEntity copyToEntityInt(RoleBean src, RoleEntity dest) {
        dest.setId(src.getId());
        dest.setRoleName(src.getRoleName());

        return dest;
    }

    @Override
    protected RoleBean copyToBeanInt(RoleEntity src, RoleBean dest) {
        dest.setId(src.getId());
        dest.setRoleName(src.getRoleName());
        return dest;
    }
    @Override
    public RoleEntity update(RoleEntity src, RoleEntity dest) {
        dest.setId(src.getId());
        dest.setRoleName(src.getRoleName());
        return dest;
    }
    public static List<RoleBean> copyRoles(Collection<RoleEntity> eRoles) {
        return Converters.ROLE_CONVERTER.copyToBeansListWithRelations(eRoles,ROLE_COPY_LEVEL);
    }
    public static RoleBean copyRole(RoleEntity eRole) {
        return Converters.ROLE_CONVERTER.copyToBeanWithRelations(eRole, null, ROLE_COPY_LEVEL);
    }
    public static RoleEntity copyRole(RoleBean bRole) {
        return Converters.ROLE_CONVERTER.copyToEntityWithRelations(bRole, null, ROLE_COPY_LEVEL);
    }
}
