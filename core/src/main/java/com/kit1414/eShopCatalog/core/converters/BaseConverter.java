package com.kit1414.eShopCatalog.core.converters;

import com.kit1414.eShopCatalog.core.beans.BaseBean;
import com.kit1414.eShopCatalog.core.entity.BaseEntity;
import org.hibernate.Session;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by kit_1414 on 01-Dec-16.
 */
abstract class BaseConverter<TBean extends BaseBean, TEntity extends BaseEntity> {

    private final Class<TBean> beanClass;
    private final Class<TEntity> entityClass;

    protected abstract TEntity copyToEntityInt(TBean src, TEntity dest);

    protected abstract TBean copyToBeanInt(TEntity src, TBean dest);

    protected void copyToEntityRelationsOnly(TBean src, TEntity dest, Integer level) {}

    protected void copyToBeanRelationsOnly(TEntity src, TBean dest, Integer level) {
    }
    public abstract TEntity update(TEntity src, TEntity dest);


        @SuppressWarnings("unchecked")
    BaseConverter() {
        Type[] types = ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments();
        this.beanClass = (Class<TBean>) types[0];
        this.entityClass = (Class<TEntity>) types[1];
    }

    protected final TBean createBean() {
        return createInstance(beanClass);
    }

    protected final TEntity createEntity() {
        return createInstance(entityClass);
    }

    public final TEntity copyToEntity(TBean bean) {
        return copyToEntity(bean, null);
    }

    public final TEntity copyToEntity(TBean bean, TEntity dest) {
        if (dest == null) {
            dest = createEntity();
        }
        return copyToEntityInt(bean, dest);
    }

    public final TBean copyToBean(TEntity src) {
        return copyToBean(src, null);
    }

    public final TBean copyToBean(TEntity src, TBean dest) {
        if (dest == null) {
            dest = createBean();
        }
        return copyToBeanInt(src, dest);
    }

    public final TEntity copyToEntityWithRelations(TBean src, TEntity dest, Integer level) {
        dest = copyToEntity(src, dest);
        final Integer newLevel = decreaseLevel(level);
        if (isCopyRelationsEnabled(newLevel)) {
            copyToEntityRelationsOnly(src, dest, newLevel);
        }
        return dest;
    }

    public final TBean copyToBeanWithRelations(TEntity src, TBean dest, Integer level) {
        dest = copyToBean(src, dest);
        final Integer newLevel = decreaseLevel(level);
        if (isCopyRelationsEnabled(newLevel)) {
            copyToBeanRelationsOnly(src, dest, newLevel);
        }
        return dest;
    }


    public final List<TEntity> copyToEntitiesListWithRelations(Collection<TBean> beans, final Integer level) {
        if (beans == null) return null;
        ArrayList<TEntity> result = new ArrayList<>(beans.size());
        beans.forEach(it -> result.add(copyToEntityWithRelations(it, null, level)));
        return result;
    }

    public final List<TBean> copyToBeansListWithRelations(Collection<TEntity> entities, final Integer level) {
        if (entities == null) return null;
        ArrayList<TBean> result = new ArrayList<>(entities.size());
        entities.forEach(it -> result.add(copyToBeanWithRelations(it, null, level)));
        return result;
    }

    private static Integer decreaseLevel(Integer level) {
        return level == null ? null : (level >= 0 ? level - 1 : level);
    }

    private boolean isCopyRelationsEnabled(Integer level) {
        return level == null || level >= 0;
    }

    private static <T> T createInstance(Class<T> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }
}
