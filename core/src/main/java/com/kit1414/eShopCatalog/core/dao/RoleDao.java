package com.kit1414.eShopCatalog.core.dao;

import com.kit1414.eShopCatalog.core.entity.RoleEntity;

/**
 * Created by kit_1414 on 29-Nov-16.
 */
public interface RoleDao extends DaoBase<RoleEntity> {
}
