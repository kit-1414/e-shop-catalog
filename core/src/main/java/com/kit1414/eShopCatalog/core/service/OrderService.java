package com.kit1414.eShopCatalog.core.service;

import com.kit1414.eShopCatalog.core.beans.OrderBean;
import com.kit1414.eShopCatalog.core.beans.OrderPositionBean;
import com.kit1414.eShopCatalog.core.beans.ProductBean;
import com.kit1414.eShopCatalog.core.exceptions.DaoException;

import java.util.List;


/**
 * Created by kit_1414 on 28-Nov-16.
 */
public interface OrderService {
    OrderBean saveOrder(OrderBean order) throws DaoException;
    OrderBean  findOrderById(long orderId)throws DaoException;
    List<OrderBean> findOrdersByCustomerId(long customerId) throws DaoException;
    List<OrderBean> findOrdersByManagerId(long customerId) throws DaoException;
    List<OrderBean> findAllOrders() throws DaoException;
    boolean removeOrder(long orderId)throws DaoException;
    List<OrderPositionBean> findAllOrderPositions(); // for testing;

    List<ProductBean> findAllProducts() throws DaoException;
    ProductBean findProductById(long id) throws DaoException;
    ProductBean saveProduct(ProductBean product) throws DaoException;
    boolean removeProduct(long productId)throws DaoException;
}
