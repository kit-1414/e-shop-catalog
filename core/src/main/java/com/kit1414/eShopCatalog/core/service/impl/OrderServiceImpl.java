package com.kit1414.eShopCatalog.core.service.impl;


import com.kit1414.eShopCatalog.core.beans.OrderBean;
import com.kit1414.eShopCatalog.core.beans.OrderPositionBean;
import com.kit1414.eShopCatalog.core.beans.ProductBean;
import com.kit1414.eShopCatalog.core.converters.OrderConverter;
import com.kit1414.eShopCatalog.core.converters.OrderPositionConverter;
import com.kit1414.eShopCatalog.core.converters.ProductConverter;
import com.kit1414.eShopCatalog.core.dao.OrderDao;
import com.kit1414.eShopCatalog.core.dao.OrderPositionDao;
import com.kit1414.eShopCatalog.core.dao.ProductDao;
import com.kit1414.eShopCatalog.core.dao.UserDao;
import com.kit1414.eShopCatalog.core.entity.OrderEntity;
import com.kit1414.eShopCatalog.core.entity.ProductEntity;
import com.kit1414.eShopCatalog.core.exceptions.DaoException;
import com.kit1414.eShopCatalog.core.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by kit_1414 on 28-Nov-16.
 */
@Service("orderService")
@Transactional
public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);


    @Autowired
    private UserDao userDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private OrderPositionDao positionDao;

    @Override
    public OrderBean saveOrder(OrderBean order) throws DaoException {
        logger.debug("saveOrder order=" + order);
        return OrderConverter.copyOrder(orderDao.createUpdate(OrderConverter.copyOrder(order)));
    }

    @Override
    public List<OrderBean> findOrdersByCustomerId(long customerId) throws DaoException {
        logger.debug("findOrdersByCustomerId customerId=" + customerId);
        return OrderConverter.copyOrders(orderDao.findByCustomerId(customerId));
    }

    @Override
    public List<OrderBean> findOrdersByManagerId(long managerId) throws DaoException {
        logger.debug("findOrdersByManagerId managerId=" + managerId);
        return OrderConverter.copyOrders(orderDao.findByManagerId(managerId));
    }

    @Override
    public List<OrderBean> findAllOrders() throws DaoException {
        logger.debug("findAllOrders()");
        return OrderConverter.copyOrders(orderDao.findAll());
    }
    @Override
    public List<OrderPositionBean> findAllOrderPositions() {
        logger.debug("findAllOrderPositions()");
        return OrderPositionConverter.copyOrderPositions(positionDao.findAll());
    }

    @Override
    public boolean removeOrder(long orderId) throws DaoException {
        logger.debug("removeOrder id=" + orderId);
        return orderDao.deleteById(orderId);
    }
    public OrderBean  findOrderById(long orderId)throws DaoException {
        logger.debug("findOrderById id=" + orderId);
        return copyOrder(orderDao.findById(orderId));
    }

    @Override
    public List<ProductBean> findAllProducts() throws DaoException {
        logger.debug("findAllProducts()");
        return ProductConverter.copyProducts(productDao.findAll());
    }

    @Override
    public ProductBean findProductById(long id) throws DaoException {
        logger.debug("findProductById id="+id);
        return copyProduct(productDao.findById(id));
    }

    @Override
    public ProductBean saveProduct(ProductBean product) throws DaoException {
        logger.debug("saveProduct id=" + product);
        return ProductConverter.copyProduct(productDao.createUpdate(ProductConverter.copyProduct(product)));
    }
    @Override
    public boolean removeProduct(long productId) throws DaoException {
        logger.debug("removeProduct productId=" + productId);
        return productDao.deleteById(productId);
    }
    private static ProductBean copyProduct(ProductEntity e) {
        return e == null ? null : ProductConverter.copyProduct(e);
    }
    private static OrderBean copyOrder(OrderEntity e) {
        return e == null ? null : OrderConverter.copyOrder(e);
    }
}
