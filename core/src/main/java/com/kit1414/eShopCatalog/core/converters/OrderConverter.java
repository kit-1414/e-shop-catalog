package com.kit1414.eShopCatalog.core.converters;

import com.kit1414.eShopCatalog.core.beans.OrderBean;
import com.kit1414.eShopCatalog.core.dao.OrderPositionDao;
import com.kit1414.eShopCatalog.core.entity.OrderEntity;
import com.kit1414.eShopCatalog.core.entity.OrderPositionEntity;
import org.hibernate.Session;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by kit_1414 on 08-Dec-16.
 */
public class OrderConverter extends BaseConverter<OrderBean, OrderEntity> {
    @Override
    protected OrderEntity copyToEntityInt(OrderBean src, OrderEntity dest) {
        dest.setId(src.getId());
        dest.setSubject(src.getSubject());
        dest.setDescription(src.getDescription());
        dest.setDeliveryAddress(src.getDeliveryAddress());
        return dest;
    }

    @Override
    protected OrderBean copyToBeanInt(OrderEntity src, OrderBean dest) {
        dest.setId(src.getId());
        dest.setSubject(src.getSubject());
        dest.setDescription(src.getDescription());
        dest.setDeliveryAddress(src.getDeliveryAddress());
        return dest;
    }
    @Override
    public OrderEntity update(OrderEntity src, OrderEntity dest) {
        dest.setId(src.getId());
        dest.setSubject(src.getSubject());
        dest.setDescription(src.getDescription());
        dest.setDeliveryAddress(src.getDeliveryAddress());
        //dest.setPositions(src.getPositions());
        return dest;
    }

    @Override
    protected void copyToEntityRelationsOnly(OrderBean src, OrderEntity dest, Integer level) {
        dest.setCustomer(Converters.USER_CONVERTER.copyToEntityWithRelations(src.getCustomer(),null,0));
        if (src.getManager()!= null) {
            dest.setManager(Converters.USER_CONVERTER.copyToEntityWithRelations(src.getManager(),null,0));
        }
        dest.setPositions(Converters.ORDER_POSITION_CONVERTER.copyToEntitiesListWithRelations(src.getPositions(),1));
    }

    @Override
    protected void copyToBeanRelationsOnly(OrderEntity src, OrderBean dest, Integer level) {
        dest.setCustomer(Converters.USER_CONVERTER.copyToBeanWithRelations(src.getCustomer(),null,0));
        if (src.getManager()!= null) {
            dest.setManager(Converters.USER_CONVERTER.copyToBeanWithRelations(src.getManager(), null, 0));
        }
        dest.setPositions(Converters.ORDER_POSITION_CONVERTER.copyToBeansListWithRelations(src.getPositions(),1));
    }

    public static List<OrderBean> copyOrders(Collection<OrderEntity> eOrders) {
        return Converters.ORDER_CONVERTER.copyToBeansListWithRelations(eOrders,0);
    }
    public static  OrderBean copyOrder(OrderEntity eOrder) {
        return Converters.ORDER_CONVERTER.copyToBeanWithRelations(eOrder, null, 1);
    }
    public static  OrderEntity copyOrder(OrderBean bOrder) {
        return Converters.ORDER_CONVERTER.copyToEntityWithRelations(bOrder, null, 1);
    }

    public List<OrderPositionEntity>  mergePositions(OrderEntity changed, OrderEntity attached) {
        Map<Long, OrderPositionEntity> oldPositions  = attached.getPositions().stream().collect(Collectors.toMap(it-> it.getProduct().getId(), it -> it));
        attached.getPositions().clear();

        for (OrderPositionEntity newPosition : changed.getPositions()) {
            OrderPositionEntity oldPosition = oldPositions.remove(newPosition.getProduct().getId());
            if (oldPosition != null) {
                Converters.ORDER_POSITION_CONVERTER.update(newPosition, oldPosition); // copy changes
                attached.getPositions().add(oldPosition);
            } else {
                newPosition.setOrder(attached);
                attached.getPositions().add(newPosition);
            }
        }
        return new ArrayList<>(oldPositions.values()); // to delete
    }

}
