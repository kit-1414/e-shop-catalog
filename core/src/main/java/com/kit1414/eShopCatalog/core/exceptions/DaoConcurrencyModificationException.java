package com.kit1414.eShopCatalog.core.exceptions;

import com.kit1414.eShopCatalog.core.enums.ErrorEnum;

/**
 * Created by kit_1414 on 07-Dec-16.
 */
public class DaoConcurrencyModificationException extends DaoException {
    public DaoConcurrencyModificationException(Class clazz, Long id, Long expectedVersion, long actualVersion) {
        super(ErrorEnum.CONCURRENCY_MODIFICATION, String.format(" Class %s id=%d, expected version=%d, actual verson = %d", clazz.getSimpleName(),id, expectedVersion, actualVersion));
    }

}
