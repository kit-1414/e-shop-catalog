package com.kit1414.eShopCatalog.core.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import static com.kit1414.eShopCatalog.core.entity.BaseEntity.E_SHOP_TABLES_PREFIX;

/**
 * Created by kit_1414 on 27-Nov-16.
 */
@Entity
@Table(name = E_SHOP_TABLES_PREFIX + "role")
public class RoleEntity extends BaseEntity {

    @Column(name="role_name", unique = true, nullable = false, length = 64)
    private String roleName;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
    private List<UserEntity> users;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }
}
