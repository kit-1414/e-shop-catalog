package com.kit1414.eShopCatalog.core.enums;

import org.springframework.http.HttpStatus;

/**
 * Created by kit_1414 on 27-Nov-16.
 */
public enum ErrorEnum {
/*
    JSON_PROCESSING_EXCEPTION(100, 555,"JSON_PROCESSING_EXCEPTION"),
    JSON_PARSE_EXCEPTION(110,      555,"JSON_PARSE_EXCEPTION"),
    JSON_MAPPING_EXCEPTION(120,    555,"JSON_MAPPING_EXCEPTION"),
    JSON_IO_EXCEPTION(130,         555, "JSON_IO_EXCEPTION"),
*/
    ELEMENT_NOT_FOUND(210,         HttpStatus.NOT_FOUND, "Entity not found"),
    NO_CONTENT(211,                HttpStatus.NO_CONTENT, "No Content founded"),
    CONTENT_CONFLICT(212,          HttpStatus.CONFLICT, "Content action conflict"),
    CONCURRENCY_MODIFICATION(213,  HttpStatus.LOCKED, "Concurrency Modification error");



    private final int code;
    private final String message;
    private final HttpStatus httpStatus;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    ErrorEnum(int code, HttpStatus httpStatus, String message) {
        this.code = code;
        this.message = message;
        this.httpStatus = httpStatus;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "code=" + code +
                ", httpStatus=" + httpStatus +
                ", message='" + message + '\'' +
                '}';
    }
}
