package com.kit1414.eShopCatalog.core.entity;

import javax.persistence.*;

/**
 * Created by kit_1414 on 27-Nov-16.
 */

@MappedSuperclass
public class BaseEntity {

    public static final String E_SHOP_TABLES_PREFIX = "eshop_";

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    //@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
