package com.kit1414.eShopCatalog.core.converters;

/**
 * Created by kit_1414 on 28-Nov-16.
 */
public interface Converters {
    RoleConverter ROLE_CONVERTER = new RoleConverter();
    UserConverter USER_CONVERTER = new UserConverter();
    ProductConverter PRODUCT_CONVERTER = new ProductConverter();
    OrderPositionConverter ORDER_POSITION_CONVERTER = new OrderPositionConverter();
    OrderConverter ORDER_CONVERTER = new OrderConverter();
}
