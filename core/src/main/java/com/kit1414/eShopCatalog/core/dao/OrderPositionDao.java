package com.kit1414.eShopCatalog.core.dao;

import com.kit1414.eShopCatalog.core.entity.OrderPositionEntity;

/**
 * Created by kit_1414 on 07-Dec-16.
 */
public interface OrderPositionDao extends DaoBase<OrderPositionEntity> {
    int deleteByOrderId(long orderId);
}
