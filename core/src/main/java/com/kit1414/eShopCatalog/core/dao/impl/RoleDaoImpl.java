package com.kit1414.eShopCatalog.core.dao.impl;

import com.kit1414.eShopCatalog.core.converters.Converters;
import com.kit1414.eShopCatalog.core.entity.UserEntity;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.kit1414.eShopCatalog.core.dao.RoleDao;
import com.kit1414.eShopCatalog.core.entity.RoleEntity;

/**
 * Created by kit_1414 on 29-Nov-16.
 */
@Repository("roleDao")
public class RoleDaoImpl extends BaseDaoImpl<RoleEntity> implements RoleDao {

    @Override
    public RoleEntity updateAttachedEntity(RoleEntity changed, RoleEntity attached) {
        return Converters.ROLE_CONVERTER.update(changed,attached);
    }
}
