package com.kit1414.eShopCatalog.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import static com.kit1414.eShopCatalog.core.entity.BaseEntity.E_SHOP_TABLES_PREFIX;

/**
 * Created by kit_1414 on 27-Nov-16.
 */
@Entity
@Table(name = E_SHOP_TABLES_PREFIX + "product")
public class ProductEntity extends BaseEntity {

    @Column(name ="product_name", length = 150, nullable = false)
    private String productName;

    @Column(name ="description", length = 150, nullable = true)
    private String description;

    @Column(name ="product_code", length = 50, nullable = false, unique = true)
    private String productCode;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}

