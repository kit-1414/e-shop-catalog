package com.kit1414.eShopCatalog.core.dao.impl;

import com.kit1414.eShopCatalog.core.converters.Converters;
import com.kit1414.eShopCatalog.core.dao.ProductDao;
import com.kit1414.eShopCatalog.core.entity.ProductEntity;
import com.kit1414.eShopCatalog.core.entity.RoleEntity;
import com.kit1414.eShopCatalog.core.entity.UserEntity;
import com.kit1414.eShopCatalog.core.exceptions.ConstraintFailException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by kit_1414 on 07-Dec-16.
 */
@Repository("productsDao")
public class ProductDaoImpl extends BaseDaoImpl<ProductEntity> implements ProductDao {
    @Override
    public ProductEntity updateAttachedEntity(ProductEntity changed, ProductEntity attached) {
        return Converters.PRODUCT_CONVERTER.update(changed,attached);
    }
    @Override
    @SuppressWarnings("unchecked")
    public ProductEntity findByProductCode(String code) {
        String hql = buildFromClause()  + " where productCode = :code";
        Query q = getSession().createQuery(hql);
        q.setString("code",code == null? null : code.trim());
        return getFirst(q.list());
    }

    protected void checkConstraint(ProductEntity newEntity) {
        checkUnique(findByProductCode(newEntity.getProductCode()), newEntity, "productCode" , newEntity.getProductCode());
    }
}
