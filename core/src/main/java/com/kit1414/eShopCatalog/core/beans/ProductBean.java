package com.kit1414.eShopCatalog.core.beans;

/**
 * Created by kit_1414 on 27-Nov-16.
 */
public class ProductBean extends BaseBean {
    private String productName;
    private String description;
    private String productCode;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}
