package com.kit1414.eShopCatalog.core.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kit1414.eShopCatalog.core.beans.RoleBean;
import com.kit1414.eShopCatalog.core.beans.UserBean;
import com.kit1414.eShopCatalog.core.converters.RoleConverter;
import com.kit1414.eShopCatalog.core.converters.UserConverter;
import com.kit1414.eShopCatalog.core.dao.RoleDao;
import com.kit1414.eShopCatalog.core.dao.UserDao;
import com.kit1414.eShopCatalog.core.entity.RoleEntity;
import com.kit1414.eShopCatalog.core.entity.UserEntity;
import com.kit1414.eShopCatalog.core.exceptions.DaoException;
import com.kit1414.eShopCatalog.core.service.AdminService;
import org.springframework.util.StringUtils;

/**
 * Created by kit_1414 on 28-Nov-16.
 */
@Service("adminService")
@Transactional
public class AdminServiceImpl implements AdminService {
    private static final Logger logger = LoggerFactory.getLogger(AdminServiceImpl.class);

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private UserDao userDao;

    @Override
    public UserBean saveUser(UserBean user) throws DaoException {
        logger.debug("saveUser user=" + user);
        UserEntity eUser = UserConverter.copyUser(user);
        userDao.createUpdate(eUser);
        logger.debug("saveUser user.id=" + user.getId());
        return UserConverter.copyUser(eUser);
    }

    @Override
    public boolean removeUser(long id) {
        logger.debug("removeUser id=" + id);
        return userDao.deleteById(id);
    }

    @Override
    public List<UserBean> findAllUsers()
    {
        logger.debug("findAllUsers");
        return UserConverter.copyUsers(userDao.findAll());
    }

    @Override
    public UserBean findUserById(long id) {
        logger.debug("findUserById id=" + id);
        return copyUser(userDao.findById(id));
    }

    @Override
    public UserBean findUserByLogin(String login) {
        logger.debug("findUserByLogin login=" + login);
        return copyUser(userDao.findByLogin(login));
    }

    @Override
    public UserBean findUserByEmail(String email) {
        logger.debug("findUserByEmail email=" + email);
        return copyUser(userDao.findByEmail(email));
    }

    @Override
    public List<RoleBean> findAllRoles() {
        logger.debug("findAllRoles");
        List<RoleEntity> list = roleDao.findAll();
        return RoleConverter.copyRoles(list);
    }
    @Override
    public RoleBean findRoleById(long roleId) {
        logger.debug("findRoleById id=" + roleId);
        return copyRole(roleDao.findById(roleId));
    }

    @Override
    public List<UserBean> findAllWithRole(long roleId) throws DaoException {
        logger.debug("findAllWithRole roleId=" + roleId);
        RoleEntity eRole = roleDao.findByIdMandatory(roleId);
        return UserConverter.copyUsers(eRole.getUsers());
    }

    @Override
    public UserBean login(String login, String md5) {
        logger.debug("login() login=" + login);
        UserEntity eUser = userDao.findByLogin(login);
        if (eUser!= null) {
            UserBean bUser = UserConverter.copyUser(eUser);
            if (!StringUtils.isEmpty(bUser.getPassMd5())) {
                if (!bUser.getPassMd5().equals(md5)) {
                    logger.debug("login() fail, wrong md5");
                    return null;
                }
            }
            logger.debug("login() OK");
            return bUser;
        }
        logger.debug("login() fail, no user founded");
        return null;
    }
    private static UserBean copyUser(UserEntity e) {
        return e == null ? null : UserConverter.copyUser(e);
    }
    private static RoleBean copyRole(RoleEntity e) {
        return e == null ? null : RoleConverter.copyRole(e);
    }
}
