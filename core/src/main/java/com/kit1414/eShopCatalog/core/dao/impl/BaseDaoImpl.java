package com.kit1414.eShopCatalog.core.dao.impl;


import java.lang.reflect.ParameterizedType;
import java.util.List;

import com.kit1414.eShopCatalog.core.entity.BaseEntity;
import com.kit1414.eShopCatalog.core.entity.UserEntity;
import com.kit1414.eShopCatalog.core.exceptions.ConstraintFailException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.kit1414.eShopCatalog.core.exceptions.ElementNotFoundException;
import com.kit1414.eShopCatalog.core.service.impl.AdminServiceImpl;

/**
 * Created by kit_1414 on 29-Nov-16.
 */
public abstract class BaseDaoImpl<T extends BaseEntity> {
    private static final Logger log = LoggerFactory.getLogger(AdminServiceImpl.class);

    private final Class<T> persistentClass;

    @Autowired
    private SessionFactory sessionFactory;

    protected abstract T updateAttachedEntity(T changed, T attached);


    @SuppressWarnings("unchecked")
    BaseDaoImpl() {
        this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    protected String getClassName() {
        return this.persistentClass.getSimpleName();
    }

    public void persist(T entity) {
        getSession().persist(entity);
    }

    public void delete(T entity) {
        getSession().delete(entity);
    }

    @SuppressWarnings("unchecked")
    public T findById(Long key) {
        return (T) getSession().get(persistentClass, key);
    }
    @SuppressWarnings("unchecked")
    public T findByIdMandatory(Long key) {
        T ePersist = (T) getSession().get(persistentClass, key);
        if (ePersist == null) {
            throw new ElementNotFoundException(this.persistentClass, key);
        }
        return ePersist;
    }

    public boolean deleteById(Long id) {
        Query q = getSession().createQuery("delete from " + getClassName() + " where id = :id");
        q.setLong("id",id);
        return q.executeUpdate() > 0;
    }

    public void clearAll() {
        getSession().createQuery("delete from " + getClassName()).executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        String hql = buildFromClause() + " u ORDER BY u.id ASC";
        log.debug("hql="+hql);
        return getSession().createQuery(hql).list();
    }
    public void refresh(T entity) {
        getSession().refresh(entity);
    }
    protected String buildFromClause() {
        return "FROM " + getClassName() + " ";
    }

    public T createUpdate(T entity) {
        Long id = entity.getId();
        if (id != null) {
            T attachedEntity = findById(entity.getId());
            if (attachedEntity == null) {
                throw new ElementNotFoundException(persistentClass,id);
            }
            checkConstraint(entity);
            entity = updateAttachedEntity(entity, attachedEntity); // unique check execute here too....
        } else {
            checkConstraint(entity);
        }
        getSession().persist(entity); // updateAttachedEntity
        getSession().flush();
        getSession().refresh(entity); // updateAttachedEntity
        return entity;
    }

    protected T getFirst(List<T> list) {
        return list!= null && list.size() > 0 ? list.iterator().next() : null;
    }
    protected final void checkUnique(T founded, T newEntity, String fieldName, String value) {
        // cover create  (id == null) && update (id defined) cases both
        if (founded != null && !founded.getId().equals(newEntity.getId())) {
            throw new ConstraintFailException(fieldName,value);
        }
    }
    protected void checkConstraint(T newEntity) {}

}