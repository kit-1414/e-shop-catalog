package com.kit1414.eShopCatalog.core.beans;

import java.util.List;

/**
 * Created by kit_1414 on 27-Nov-16.
 */
public class UserBean extends BaseBean{
    private String login;
    private String email;
    private String firstName;
    private String lastName;
    private String passMd5;
    private String description;

    private List<RoleBean> roles;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassMd5() {
        return passMd5;
    }

    public void setPassMd5(String passMd5) {
        this.passMd5 = passMd5;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<RoleBean> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleBean> roles) {
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
