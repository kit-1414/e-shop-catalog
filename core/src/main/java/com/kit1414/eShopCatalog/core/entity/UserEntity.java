package com.kit1414.eShopCatalog.core.entity;



import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

import static com.kit1414.eShopCatalog.core.entity.BaseEntity.E_SHOP_TABLES_PREFIX;

/**
 * Created by kit_1414 on 27-Nov-16.
 */
@Entity
@Table(name = E_SHOP_TABLES_PREFIX + "user")
public class UserEntity extends BaseEntity {

    @Column(name ="login", length = 150, nullable = false, unique = true)
    private String login;

    @Column(name ="email", length = 150, nullable = false, unique = true)
    private String email;

    @Column(name ="first_name", length = 150)
    private String firstName;

    @Column(name ="last_name", length = 150)
    private String lastName;

    @Column(name ="pass_md5", length = 150)
    private String passMd5;

    @Column(name ="description", length = 1000)
    private String description;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = E_SHOP_TABLES_PREFIX + "user_role",
            joinColumns = {@JoinColumn(name = "user_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "role_id", nullable = false, updatable = false),
    })
    private List<RoleEntity> roles;


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassMd5() {
        return passMd5;
    }

    public void setPassMd5(String passMd5) {
        this.passMd5 = passMd5;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleEntity> roles) {
        this.roles = roles;
    }
}
