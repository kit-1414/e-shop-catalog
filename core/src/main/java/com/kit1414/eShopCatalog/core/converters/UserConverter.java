package com.kit1414.eShopCatalog.core.converters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;
import org.springframework.util.CollectionUtils;

import com.kit1414.eShopCatalog.core.beans.RoleBean;
import com.kit1414.eShopCatalog.core.beans.UserBean;
import com.kit1414.eShopCatalog.core.entity.RoleEntity;
import com.kit1414.eShopCatalog.core.entity.UserEntity;

/**
 * Created by kit_1414 on 01-Dec-16.
 */
public class UserConverter extends BaseConverter<UserBean, UserEntity> {

    @Override
    protected UserEntity copyToEntityInt(UserBean src, UserEntity dest) {
        dest.setId(src.getId());
        dest.setDescription(src.getDescription());
        if (src.getLogin()!= null) {
            dest.setLogin(src.getLogin().toLowerCase().trim());
        }
        if (src.getEmail()!= null) {
            dest.setEmail(src.getEmail().toLowerCase().trim());
        }
        dest.setFirstName(src.getFirstName());
        dest.setLastName(src.getLastName());
        dest.setPassMd5(src.getPassMd5());

        return dest;
    }

    @Override
    protected UserBean copyToBeanInt(UserEntity src, UserBean dest) {
        dest.setId(src.getId());
        dest.setDescription(src.getDescription());
        dest.setEmail(src.getEmail());
        dest.setFirstName(src.getFirstName());
        dest.setLastName(src.getLastName());
        dest.setLogin(src.getLogin());
        dest.setPassMd5(src.getPassMd5());

        return dest;
    }
    @Override
    public UserEntity update(UserEntity src, UserEntity dest) {
        dest.setId(src.getId());
        dest.setDescription(src.getDescription());
        dest.setLogin(src.getLogin());
        dest.setEmail(src.getEmail());
        dest.setFirstName(src.getFirstName());
        dest.setLastName(src.getLastName());
        dest.setPassMd5(src.getPassMd5());
        dest.setRoles(src.getRoles());

        return dest;
    }

    protected void copyToEntityRelationsOnly(UserBean src, UserEntity dest, Integer level) {
        // Copy roles
        int rolesSize = CollectionUtils.isEmpty(src.getRoles()) ? 0 : src.getRoles().size();
        ArrayList<RoleEntity> roles = new ArrayList<>(rolesSize);
        if (rolesSize > 0) {
            src.getRoles().forEach(it -> roles.add(copyRoleLight(it))); // just ID, to updateAttachedEntity relation user->role
        }
        dest.setRoles(roles);
    }
    protected void copyToBeanRelationsOnly(UserEntity src, UserBean dest, Integer level) {
        // Copy roles
        int rolesSize = CollectionUtils.isEmpty(src.getRoles()) ? 0 : src.getRoles().size();
        ArrayList<RoleBean> roles = new ArrayList<>(rolesSize);
        if (rolesSize > 0) {
            src.getRoles().forEach(it -> roles.add(Converters.ROLE_CONVERTER.copyToBeanWithRelations(it, null, level)));
        }
        dest.setRoles(roles);
    }

    public static List<UserBean> copyUsers(Collection<UserEntity> eUsers) {
        return Converters.USER_CONVERTER.copyToBeansListWithRelations(eUsers,0);
    }
    public static  UserBean copyUser(UserEntity eUser) {
        return Converters.USER_CONVERTER.copyToBeanWithRelations(eUser, null, 1);
    }
    public static  UserEntity copyUser(UserBean bUser) {
        return Converters.USER_CONVERTER.copyToEntityWithRelations(bUser, null, 1);
    }

    private static RoleEntity copyRoleLight(RoleBean bRole) {
        RoleEntity eRole = new RoleEntity();
        eRole.setId(bRole.getId());
        return eRole;
    }
}
