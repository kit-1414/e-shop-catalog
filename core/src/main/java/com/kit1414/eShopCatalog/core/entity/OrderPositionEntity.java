package com.kit1414.eShopCatalog.core.entity;

import javax.persistence.*;

import java.util.List;

import static com.kit1414.eShopCatalog.core.entity.BaseEntity.E_SHOP_TABLES_PREFIX;

/**
 * Created by kit_1414 on 07-Dec-16.
 */

@Entity
@Table(name = E_SHOP_TABLES_PREFIX + "order_position")
public class OrderPositionEntity extends BaseEntity {

    @ManyToOne
    @JoinColumn(name="product_id")
    private ProductEntity product;

    @Column(name ="product_amount")
    private int productAmount;

    @Column(name ="product_price")
    private int productPrice;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = OrderEntity.class, optional = true)
    private OrderEntity order;

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }

    public int getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(int productAmount) {
        this.productAmount = productAmount;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }
}
