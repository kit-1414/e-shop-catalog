package com.kit1414.eShopCatalog.core.beans;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by kit_1414 on 27-Nov-16.
 */
public class BaseBean {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseBean)) return false;

        BaseBean baseBean = (BaseBean) o;

        return id != null ? id.equals(baseBean.id) : baseBean.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
         return ToStringBuilder.reflectionToString(this);
    }
}
