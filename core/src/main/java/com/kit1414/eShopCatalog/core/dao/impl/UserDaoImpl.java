package com.kit1414.eShopCatalog.core.dao.impl;

import java.util.List;

import com.kit1414.eShopCatalog.core.converters.Converters;
import com.kit1414.eShopCatalog.core.converters.UserConverter;
import com.kit1414.eShopCatalog.core.exceptions.ConstraintFailException;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.kit1414.eShopCatalog.core.dao.UserDao;
import com.kit1414.eShopCatalog.core.entity.UserEntity;

import javax.jws.soap.SOAPBinding;

/**
 * Created by kit_1414 on 29-Nov-16.
 */
@Repository("userDao")
public class UserDaoImpl extends BaseDaoImpl<UserEntity> implements UserDao {

    @Override
    public UserEntity updateAttachedEntity(UserEntity  changed, UserEntity  attached) {
        return Converters.USER_CONVERTER.update(changed,attached);
    }
    @Override
    @SuppressWarnings("unchecked")
    public UserEntity findByLogin(String login) {
        String hql = buildFromClause()  + " where login = :login";
        Query q = getSession().createQuery(hql);
        q.setString("login",login == null ? null : login.toLowerCase());
        return getFirst(q.list());
    }

    @Override
    @SuppressWarnings("unchecked")
    public UserEntity findByEmail(String email) {
        String hql = buildFromClause()  + " where email = :email";
        Query q = getSession().createQuery(hql);
        q.setString("email",email == null ? null : email.toLowerCase());
        return getFirst(q.list());
    }

    protected void checkConstraint(UserEntity newEntity) {
        String login = newEntity.getLogin();
        checkUnique(findByLogin(login), newEntity, "login" , login);

        String email = newEntity.getEmail();
        checkUnique(findByEmail(email), newEntity, "email" , email);
    }
}
