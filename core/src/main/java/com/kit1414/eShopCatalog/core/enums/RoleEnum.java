package com.kit1414.eShopCatalog.core.enums;

import com.kit1414.eShopCatalog.core.beans.RoleBean;

/**
 * Created by kit_1414 on 27-Nov-16.
 */
public enum RoleEnum {

    ADMIN(10,"ADMIN"),
    MANAGER(20,"MANAGER"),
    CUSTOMER(30,"CUSTOMER"),
    GUEST(40,"Guest");

    private final long id;
    private final String name;

    RoleEnum(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    public static RoleEnum valueOf(long id) {
        for (RoleEnum r : RoleEnum.values()) {
            if (r.getId() == id) {
                return r;
            }
        }
        return null;
    }
    public RoleBean getBean() {
        RoleBean r = new RoleBean();
        r.setId(id);
        r.setRoleName(name);
        return r;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
