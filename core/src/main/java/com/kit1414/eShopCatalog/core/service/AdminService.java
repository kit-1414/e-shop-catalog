package com.kit1414.eShopCatalog.core.service;

import java.util.List;

import com.kit1414.eShopCatalog.core.beans.RoleBean;
import com.kit1414.eShopCatalog.core.beans.UserBean;
import com.kit1414.eShopCatalog.core.exceptions.DaoException;

/**
 * Created by kit_1414 on 28-Nov-16.
 */
public interface AdminService {

    UserBean login(String login, String md5);

    UserBean saveUser(UserBean user) throws DaoException;
    boolean  removeUser(long id);

    List<UserBean> findAllUsers();
    UserBean findUserById(long id);
    UserBean findUserByLogin(String login);
    UserBean findUserByEmail(String email);

    List<RoleBean> findAllRoles();
    RoleBean findRoleById(long id);
    List<UserBean> findAllWithRole(long roleId) throws DaoException;
}
