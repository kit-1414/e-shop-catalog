package com.kit1414.eShopCatalog.core.beans;

/**
 * Created by kit_1414 on 07-Dec-16.
 */
public class OrderPositionBean extends BaseBean {
    private ProductBean product;
    private int productAmount;
    private int productPrice;


    public ProductBean getProduct() {
        return product;
    }

    public void setProduct(ProductBean product) {
        this.product = product;
    }

    public int getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(int productAmount) {
        this.productAmount = productAmount;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }
}
