package com.kit1414.eShopCatalog.core.exceptions;

import com.kit1414.eShopCatalog.core.enums.ErrorEnum;
import org.springframework.http.HttpStatus;

/**
 * Created by kit_1414 on 28-Nov-16.
 */
public class DaoException extends RuntimeException {
    private final ErrorEnum error;
    private final String message;

    public ErrorEnum getError() {
        return error;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return error.getHttpStatus();
    }

    public DaoException(ErrorEnum error, String message) {
        super();
        this.error = error;
        this.message = message;
    }
}
