package com.kit1414.eShopCatalog.core.exceptions;

import com.kit1414.eShopCatalog.core.enums.ErrorEnum;

/**
 * Created by aamelin on 06.12.2016.
 */
public class ElementNotFoundException extends DaoException {
    public ElementNotFoundException(Class clazz, Long id) {
        super(ErrorEnum.ELEMENT_NOT_FOUND, clazz.getSimpleName() + " not found by id=" + id);
    }
    public ElementNotFoundException() {
        super(ErrorEnum.ELEMENT_NOT_FOUND, "Element Not Found");
    }
}
