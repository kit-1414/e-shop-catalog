package com.kit1414.eShopCatalog.core.dao;

import com.kit1414.eShopCatalog.core.entity.OrderEntity;

import java.util.List;

/**
 * Created by kit_1414 on 07-Dec-16.
 */
public interface OrderDao extends DaoBase<OrderEntity> {
    List<OrderEntity> findByManagerId(long managerId);
    List<OrderEntity> findByCustomerId(long customerId);
}
