package com.kit1414.eShopCatalog.core.dao;

import com.kit1414.eShopCatalog.core.entity.UserEntity;

/**
 * Created by kit_1414 on 29-Nov-16.
 */
public interface UserDao extends DaoBase<UserEntity> {
    UserEntity findByLogin(String login);
    UserEntity findByEmail(String email);
}
