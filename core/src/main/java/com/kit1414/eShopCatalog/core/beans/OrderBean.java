package com.kit1414.eShopCatalog.core.beans;

import java.util.List;

/**
 * Created by kit_1414 on 27-Nov-16.
 */
public class OrderBean extends BaseBean {
    private List<OrderPositionBean> positions;
    private UserBean customer;
    private UserBean manager;

    private String deliveryAddress;
    private String description;
    private String subject;

    public List<OrderPositionBean> getPositions() {
        return positions;
    }

    public void setPositions(List<OrderPositionBean> positions) {
        this.positions = positions;
    }

    public UserBean getCustomer() {
        return customer;
    }

    public void setCustomer(UserBean customer) {
        this.customer = customer;
    }

    public UserBean getManager() {
        return manager;
    }

    public void setManager(UserBean manager) {
        this.manager = manager;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
