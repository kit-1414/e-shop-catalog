package com.kit1414.eShopCatalog.core.dao.impl;

import com.kit1414.eShopCatalog.core.converters.Converters;
import com.kit1414.eShopCatalog.core.dao.OrderPositionDao;
import com.kit1414.eShopCatalog.core.entity.OrderPositionEntity;
import com.kit1414.eShopCatalog.core.entity.ProductEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by kit_1414 on 07-Dec-16.
 */
@Repository("orderPositionDao")
public class OrderPositionDaoImpl extends BaseDaoImpl <OrderPositionEntity> implements OrderPositionDao {

    @Override
    public OrderPositionEntity updateAttachedEntity(OrderPositionEntity changed, OrderPositionEntity attached) {
        return Converters.ORDER_POSITION_CONVERTER.update(changed,attached);
    }
    @Override
    public int deleteByOrderId(long orderId) {
        Query q = getSession().createQuery("delete from " + getClassName() + " op where op.order.id = :id");
        q.setLong("id",orderId);
        return q.executeUpdate();
    }
}
