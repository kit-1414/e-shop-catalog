package com.kit1414.eShopCatalog.core.exceptions;

import com.kit1414.eShopCatalog.core.enums.ErrorEnum;

/**
 * Created by kit_1414 on 10-Feb-17.
 */
public class NoContentFoundException extends DaoException {
    public NoContentFoundException() {
        super(ErrorEnum.NO_CONTENT, "NoContentFoundException");
    }
}
