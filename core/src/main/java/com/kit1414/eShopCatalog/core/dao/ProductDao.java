package com.kit1414.eShopCatalog.core.dao;

import com.kit1414.eShopCatalog.core.entity.ProductEntity;

/**
 * Created by kit_1414 on 07-Dec-16.
 */
public interface ProductDao extends DaoBase<ProductEntity>  {
    ProductEntity findByProductCode(String code);
}
