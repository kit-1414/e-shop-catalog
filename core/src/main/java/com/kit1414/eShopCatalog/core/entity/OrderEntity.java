package com.kit1414.eShopCatalog.core.entity;


import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

import static com.kit1414.eShopCatalog.core.entity.BaseEntity.E_SHOP_TABLES_PREFIX;

/**
 * Created by kit_1414 on 27-Nov-16.
 */

@Entity
@Table(name = E_SHOP_TABLES_PREFIX + "order")
public class OrderEntity extends BaseEntity {

//    @OneToMany(cascade ={CascadeType.PERSIST, CascadeType.REMOVE},fetch = FetchType.LAZY)
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="order_id")
    private List<OrderPositionEntity> positions;

    @ManyToOne(optional = false)
    @JoinColumn(name="customer_id")
    private UserEntity customer;

    @ManyToOne
    @JoinColumn(name="manager_id")
    private UserEntity manager;

    @Column(name ="delivery_address", length = 250, nullable = false)
    private String deliveryAddress;

    @Column(name ="description", length = 500)
    private String description;

    @Column(name ="subject", length = 200)
    private String subject;

    public List<OrderPositionEntity> getPositions() {
        return positions;
    }

    public void setPositions(List<OrderPositionEntity> positions) {
        this.positions = positions;
    }


    public UserEntity getCustomer() {
        return customer;
    }

    public void setCustomer(UserEntity customer) {
        this.customer = customer;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public UserEntity getManager() {
        return manager;
    }

    public void setManager(UserEntity manager) {
        this.manager = manager;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
