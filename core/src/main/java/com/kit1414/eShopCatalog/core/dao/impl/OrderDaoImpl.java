package com.kit1414.eShopCatalog.core.dao.impl;

import com.kit1414.eShopCatalog.core.converters.Converters;
import com.kit1414.eShopCatalog.core.dao.OrderDao;
import com.kit1414.eShopCatalog.core.dao.OrderPositionDao;
import com.kit1414.eShopCatalog.core.dao.UserDao;
import com.kit1414.eShopCatalog.core.entity.OrderEntity;
import com.kit1414.eShopCatalog.core.entity.OrderPositionEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by kit_1414 on 07-Dec-16.
 */
@Repository("orderDao")
public class OrderDaoImpl extends BaseDaoImpl<OrderEntity> implements OrderDao {

    @Autowired
    private OrderPositionDao positionDao;

    @Autowired
    private UserDao userDao;

    @Override
    @SuppressWarnings(value = "unchecked")
    public List<OrderEntity> findByManagerId(long managerId) {
        String hql = buildFromClause() + " o where o.manager.id = :managerId";
        Query q = getSession().createQuery(hql);
        q.setLong("managerId", managerId);
        return (List<OrderEntity>) q.list();
    }

    @Override
    @SuppressWarnings(value = "unchecked")
    public List<OrderEntity> findByCustomerId(long customterId) {
        String hql = buildFromClause() + " o where o.customer.id = :customerId";
        Query q = getSession().createQuery(hql);
        q.setLong("customerId", customterId);
        return (List<OrderEntity>) q.list();
    }

    @Override
    public OrderEntity updateAttachedEntity(OrderEntity changed, OrderEntity attached) {
        // merge old && new oldPositions

        if (changed.getManager() == null) {
            attached.setManager(null);
        } else {
            if (attached.getManager() == null || !attached.getManager().equals(changed.getManager())) {
                attached.setManager(userDao.findById(changed.getManager().getId()));
            }
        }

        List<OrderPositionEntity> toDelete = Converters.ORDER_CONVERTER.mergePositions(changed, attached);
        attached.getPositions().forEach(it -> positionDao.persist(it)); // create or updateAttachedEntity
        toDelete.forEach(it -> positionDao.delete(it));
        getSession().flush();

        return Converters.ORDER_CONVERTER.update(changed,attached);
    }

    @Override
    public boolean deleteById(Long id) {
        positionDao.deleteByOrderId(id);
        return super.deleteById(id);
    }

}