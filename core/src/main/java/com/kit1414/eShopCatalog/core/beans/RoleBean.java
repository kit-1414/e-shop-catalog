package com.kit1414.eShopCatalog.core.beans;

import java.util.List;

/**
 * Created by kit_1414 on 28-Nov-16.
 */
public class RoleBean extends BaseBean {
    private String roleName;

    public RoleBean() {
    }

    public RoleBean(long id) {
        setId(id);
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
