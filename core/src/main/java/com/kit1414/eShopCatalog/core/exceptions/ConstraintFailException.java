package com.kit1414.eShopCatalog.core.exceptions;

import com.kit1414.eShopCatalog.core.enums.ErrorEnum;

import static com.kit1414.eShopCatalog.core.enums.ErrorEnum.CONTENT_CONFLICT;

/**
 * Created by kit_1414 on 15-Feb-17.
 */
public class ConstraintFailException extends DaoException {
    public ConstraintFailException(String fieldName, String value) {
        super(CONTENT_CONFLICT, " field " + fieldName + " value duplicated: " + value);
    }
}
