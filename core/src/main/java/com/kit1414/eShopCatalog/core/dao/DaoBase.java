package com.kit1414.eShopCatalog.core.dao;

import com.kit1414.eShopCatalog.core.entity.BaseEntity;

import java.util.List;

/**
 * Created by kit_1414 on 29-Nov-16.
 */
public interface DaoBase<T extends BaseEntity> {
    void delete(T entity);
    void persist(T entity);

    T createUpdate(T entity);
    boolean deleteById(Long id);
    T findById(Long id);
    T findByIdMandatory(Long id);
    List<T> findAll();
    void refresh(T Entity);
    void clearAll();
}
