package com.kit1414.eShopCatalog.core.converters;

import com.kit1414.eShopCatalog.core.beans.ProductBean;
import com.kit1414.eShopCatalog.core.entity.ProductEntity;
import org.hibernate.Session;

import java.util.Collection;
import java.util.List;

/**
 * Created by kit_1414 on 07-Dec-16.
 */
public class ProductConverter extends BaseConverter<ProductBean, ProductEntity> {
    public static final int PRODUCT_COPY_LEVEL = 0;

    @Override
    protected ProductEntity copyToEntityInt(ProductBean src, ProductEntity dest) {
        dest.setId(src.getId());
        if (src.getProductCode()!= null) {
            dest.setProductCode(src.getProductCode().trim());
        }
        dest.setDescription(src.getDescription());
        dest.setProductName(src.getProductName());
        return dest;
    }

    @Override
    protected ProductBean copyToBeanInt(ProductEntity src, ProductBean dest) {
        dest.setId(src.getId());
        dest.setDescription(src.getDescription());
        dest.setProductCode(src.getProductCode());
        dest.setProductName(src.getProductName());
        return dest;
    }
    @Override
    public ProductEntity update(ProductEntity src, ProductEntity dest) {
        dest.setId(src.getId());
        dest.setProductCode(src.getProductCode());
        dest.setDescription(src.getDescription());
        dest.setProductName(src.getProductName());
        return dest;
    }
    public static List<ProductBean> copyProducts(Collection<ProductEntity> eProducts) {
        return Converters.PRODUCT_CONVERTER.copyToBeansListWithRelations(eProducts,PRODUCT_COPY_LEVEL);
    }
    public static  ProductBean copyProduct(ProductEntity eProduct) {
        return Converters.PRODUCT_CONVERTER.copyToBeanWithRelations(eProduct,null, PRODUCT_COPY_LEVEL);
    }
    public static  ProductEntity copyProduct(ProductBean bProduct) {
        return Converters.PRODUCT_CONVERTER.copyToEntityWithRelations(bProduct,null, PRODUCT_COPY_LEVEL);
    }
}
