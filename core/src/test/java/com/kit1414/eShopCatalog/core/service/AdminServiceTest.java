package com.kit1414.eShopCatalog.core.service;

import java.util.*;

import com.google.common.collect.Sets;
import com.kit1414.eShopCatalog.core.exceptions.ConstraintFailException;
import org.apache.commons.lang.StringUtils;
import org.hibernate.PropertyValueException;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.kit1414.eShopCatalog.core.beans.RoleBean;
import com.kit1414.eShopCatalog.core.beans.UserBean;
import com.kit1414.eShopCatalog.core.enums.RoleEnum;


/**
 * Created by kit_1414 on 01-Dec-16.
 */
public class AdminServiceTest extends BaseServiceTest {
    private static final Logger log = LoggerFactory.getLogger(AdminServiceTest.class);

    public static final String ADMIN_EMAIL = "admin-email@gmail.com";
    public static final String ADMIN_LOGIN = "admin";
    public static final String ADMIN_PASS_MD5 = "f6fdffe48c908deb0f4c3bd36c032e72";
    public static final String ADMIN_PASS = "adminadmin";
    public static final long ADMIN_ID = 1;

    @Autowired
    AdminService adminService;

    @Test
    public void testFindAllUsersTest() {
        List<UserBean> users = adminService.findAllUsers();
        log.info(String.format(" %d user(s) founded", users.size()));
        users.forEach(it -> {
            checkRolesEmpty(it);
            log.info(it.toString());
        });
        Assert.assertTrue(users.size() > 0);
    }
    @Test
    public void testFinUserByIdTest() {
        UserBean user = adminService.findUserById(ADMIN_ID);
        Assert.assertTrue(user.getRoles().size() > 0);
        checkRolesNotEmpty(user);
        log.info("User founded " + user);
        Assert.assertNotNull(user);
    }

    @Test
    public void testFindUserByLoginTest() {
        UserBean user = adminService.findUserByLogin(ADMIN_LOGIN);
        Assert.assertTrue(user.getRoles().size() > 0);
        checkRolesNotEmpty(user);
        log.info("User founded " + user);
        Assert.assertNotNull(user);
    }
    @Test
    public void findUsersByRoleTest() {
        List<UserBean> users = adminService.findAllWithRole(RoleEnum.ADMIN.getId());
        log.info(String.format(" %d user(s) founded", users.size()));
        users.forEach(it -> {
            log.info(it.toString());
            checkRolesEmpty(it);
        });
        Assert.assertTrue(users.size() > 0);
    }

    @Test
    public void adminLoginOkTest() {
        UserBean admin = adminService.login(ADMIN_LOGIN, ADMIN_PASS_MD5);
        Assert.assertNotNull(admin);
        checkRolesNotEmpty(admin);
        Optional<RoleBean> adminRoleOptional = admin.getRoles().stream().filter(it -> RoleEnum.ADMIN.getId() == it.getId()).findFirst();
        Assert.assertTrue(adminRoleOptional.isPresent());
    }

    @Test
    public void adminLoginFailTest() {
        Assert.assertNull(adminService.login(ADMIN_LOGIN, ADMIN_PASS_MD5 + "-"));
    }

    @Test
    public void sameNewUserOkTest() {
        UserBean newUser = getNewUser(1);
        UserBean saved = adminService.saveUser(newUser);
        log.info("Save done:" + saved);
        checkRolesNotEmpty(saved);
        Assert.assertNotNull(saved.getId());
        Assert.assertEquals(1, saved.getRoles().size());
        RoleBean role = saved.getRoles().get(0);
        Assert.assertEquals(role.getId().longValue(), RoleEnum.MANAGER.getId());
    }

    @Test(expectedExceptions = PropertyValueException.class)
    public void sameLoginEmptyTest() {
        UserBean newUser = getNewUser(1);
        newUser.setLogin(null);
        adminService.saveUser(newUser);
    }

    @Test(expectedExceptions = PropertyValueException.class)
    public void sameEmailTest() {
        UserBean newUser = getNewUser(1);
        newUser.setEmail(null);
        adminService.saveUser(newUser);
    }

    @Test(expectedExceptions = ConstraintFailException.class)
    public void sameEmailDuplicateTest() {
        UserBean admin = adminService.findUserById(ADMIN_ID);
        UserBean newUser = getNewUser(1);
        newUser.setEmail(admin.getEmail().toUpperCase());
        adminService.saveUser(newUser);
    }

    @Test(expectedExceptions = ConstraintFailException.class)
    public void sameLoginDuplicateTest() {
        UserBean admin = adminService.findUserById(ADMIN_ID);
        UserBean newUser = getNewUser(1);
        newUser.setLogin(admin.getLogin().toUpperCase());
        adminService.saveUser(newUser);
    }

    @Test
    public void crudUserTest() {

        List<UserBean> oldUsers = adminService.findAllUsers();
        Assert.assertEquals(oldUsers.size(), 5);
        Assert.assertEquals(oldUsers.size(), 5);

        UserBean newUser = getNewUser(1);
        log.info("To save:" + newUser);

        UserBean saved = adminService.saveUser(newUser);
        checkRolesNotEmpty(saved );
        log.info("Save done:" + saved);

        UserBean searchResult = null;


        searchResult = adminService.findUserById(saved.getId());
        Assert.assertNotNull(searchResult);
        checkUsersSame(searchResult, saved);
        checkRolesNotEmpty(searchResult);

        searchResult = adminService.findUserByEmail(saved.getEmail().toUpperCase());
        Assert.assertNotNull(searchResult);
        checkRolesNotEmpty(searchResult);
        checkUsersSame(searchResult, saved);

        searchResult = adminService.findUserByLogin(saved.getLogin().toUpperCase());
        checkRolesNotEmpty(searchResult);
        Assert.assertNotNull(searchResult);
        checkUsersSame(searchResult, saved);


        List<UserBean> newUsers = adminService.findAllUsers();
        checkRolesEmpty(newUsers);
        Assert.assertEquals(newUsers.size(), oldUsers.size() + 1);

        saved.setDescription("ssssss");
        adminService.saveUser(saved);

        searchResult = adminService.findUserById(saved.getId());
        Assert.assertNotNull(searchResult);
        checkRolesNotEmpty(searchResult);
        checkUsersSame(searchResult, saved);

        Assert.assertTrue(adminService.removeUser(searchResult.getId()));

        newUsers = adminService.findAllUsers();
        Assert.assertEquals(newUsers.size(), oldUsers.size());
        checkRolesEmpty(newUsers);

        Assert.assertFalse(adminService.removeUser(searchResult.getId()));

    }

    @Test
    public void userRolesTest() {
        UserBean newUser = getNewUser(1);
        log.info("To save:" + newUser);
        newUser.setRoles(new ArrayList<>());
        newUser.getRoles().add(new RoleBean(RoleEnum.MANAGER.getId()));

        UserBean saved = adminService.saveUser(newUser);
        log.info("Save done:" + saved);
        long userId = saved.getId();

        checkRolesNotEmpty(saved);
        Assert.assertEquals(saved.getRoles().size(), 1);
        RoleBean role = saved.getRoles().get(0);
        Assert.assertEquals(role.getId().longValue(), RoleEnum.MANAGER.getId());
        saved.getRoles().add(new RoleBean(RoleEnum.CUSTOMER.getId()));

        saved = adminService.saveUser(saved);
        UserBean search = adminService.findUserById(userId);
        checkRolesNotEmpty(search);

        List<RoleBean> allRoles1 = adminService.findAllRoles();
        Assert.assertEquals(search.getRoles().size(), 2);
        for (RoleBean r : search.getRoles()) {
            Assert.assertTrue(StringUtils.isNotEmpty(r.getRoleName()));
        }
        HashSet<RoleBean> rolesSet = new HashSet<>(search.getRoles());
        Set<RoleBean> expSet = Sets.newHashSet(new RoleBean(RoleEnum.CUSTOMER.getId()), new RoleBean(RoleEnum.MANAGER.getId()));
        rolesSet.removeAll(expSet);
        Assert.assertEquals(rolesSet.size(), 0);

        List<RoleBean> allRoles = adminService.findAllRoles();
        Assert.assertEquals(allRoles.size(), RoleEnum.values().length);


    }

    private void checkRolesEmpty(UserBean user) {
        Assert.assertNull(user.getRoles());
    }
    private void checkRolesEmpty(Collection<UserBean> users) {
        users.forEach(it -> checkRolesEmpty(it));
    }

    private void checkRolesNotEmpty(UserBean user) {
        Assert.assertNotNull(user.getRoles());
        for (RoleBean r : user.getRoles()) {
            Assert.assertNotNull(r.getId());
            Assert.assertTrue(StringUtils.isNotEmpty(r.getRoleName()));
        }
    }

    private static UserBean getNewUser(long l) {
        UserBean newUser = new UserBean();
        newUser.setLogin("login-" + l);
        newUser.setFirstName("first name " + l);
        newUser.setLastName("last name " + l);
        newUser.setEmail(l + "-email@gmail.com");
        newUser.setPassMd5("zzzzzzzzzz");
        newUser.setDescription("new user " + l);
        newUser.setRoles(new ArrayList<>());
        newUser.getRoles().add(new RoleBean(RoleEnum.MANAGER.getId()));
        return newUser;
    }
    private void checkUsersSame(UserBean p1, UserBean p2) {
        Assert.assertEquals(p1.getId(), p2.getId());
        Assert.assertEquals(p1.getDescription(), p2.getDescription());
        Assert.assertEquals(p1.getEmail().toLowerCase(), p2.getEmail().toLowerCase());
        Assert.assertEquals(p1.getLogin().toLowerCase(), p2.getLogin().toLowerCase());
        Assert.assertEquals(p1.getFirstName(), p2.getFirstName());
        Assert.assertEquals(p1.getLastName(), p2.getLastName());
        Assert.assertEquals(p1.getPassMd5(), p2.getPassMd5());

        HashSet<RoleBean> roles1 = new HashSet<>(p1.getRoles());
        HashSet<RoleBean> roles2 = new HashSet<>(p2.getRoles());

        Assert.assertEquals(roles1.size(), roles2.size());
        roles1.removeAll(roles2);
        Assert.assertEquals(roles1.size(), 0);
    }

}