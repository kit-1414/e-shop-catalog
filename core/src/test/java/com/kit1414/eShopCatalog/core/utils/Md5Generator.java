package com.kit1414.eShopCatalog.core.utils;

import org.apache.commons.codec.binary.Hex;

import java.nio.charset.Charset;
import java.security.MessageDigest;

/**
 * Created by kit_1414 on 03-Mar-17.
 */
public class Md5Generator {
    private static String[] passwds= {
            "adminadmin",
            "qwerty"
    };
    public static void main(String[] args) throws  Exception{
        for (String passwd : passwds) {
            String md5 = md5(passwd);
            System.out.println(String.format("passwd=[%s], md5=[%s]",passwd,md5));
        }
    }
    private static String md5(String string)  throws Exception {
        final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.reset();
        messageDigest.update(string.getBytes(Charset.forName("UTF8")));
        final byte[] resultByte = messageDigest.digest();
        final String result = new String(Hex.encodeHex(resultByte));
        return result;
    }
}
