package com.kit1414.eShopCatalog.core.service;

import com.kit1414.eShopCatalog.core.beans.OrderBean;
import com.kit1414.eShopCatalog.core.beans.OrderPositionBean;
import com.kit1414.eShopCatalog.core.beans.ProductBean;
import com.kit1414.eShopCatalog.core.beans.UserBean;
import com.kit1414.eShopCatalog.core.enums.RoleEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kit_1414 on 08-Dec-16.
 */
public class OrdersTest extends  BaseServiceTest {
    private static final Logger log = LoggerFactory.getLogger(OrdersTest.class);

    @Autowired
    AdminService adminService;

    @Autowired
    OrderService orderService;

    @Test
    public void findAllOrders() {
        List<OrderBean> orders = orderService.findAllOrders();
        log.info(String.format(" %d orders founded", orders.size()));
        orders.forEach(it -> log.info(it.toString()));
        Assert.assertEquals(3,orders.size());
    }

    @Test
    public void findOrderById() {
        OrderBean order = orderService.findOrderById(1);
        log.info("Order founded: " + order);
        Assert.assertEquals(order.getId().longValue(),1);
    }

    @Test
    public void findOrdersByManagerId() {
        List<OrderBean> orders = orderService.findOrdersByManagerId(21);
        log.info(String.format(" %d orders founded", orders.size()));
        orders.forEach(it -> log.info(it.toString()));
        Assert.assertEquals(1,orders.size());
    }
    @Test
    public void findOrdersByCustomerId() {
        List<OrderBean> orders = orderService.findOrdersByCustomerId(30);
        log.info(String.format(" %d orders founded", orders.size()));
        orders.forEach(it -> log.info(it.toString()));
        Assert.assertEquals(2,orders.size());
    }
    @Test
    public void crudOrderTest() {

        List<ProductBean> products = orderService.findAllProducts();
        List<UserBean> users = adminService.findAllWithRole(RoleEnum.CUSTOMER.getId());
        List<UserBean> managers = adminService.findAllWithRole(RoleEnum.MANAGER.getId());
        List<OrderBean> oldOrders = orderService.findAllOrders();
        List<OrderPositionBean> oldPositions = orderService.findAllOrderPositions();

        Assert.assertTrue(users.size() > 0);
        Assert.assertTrue(managers.size() > 0);
        UserBean customer = users.get(0);
        UserBean manager = managers.get(0);

        Assert.assertTrue(products.size() >= 3);
        ProductBean product1 = products.get(0);
        ProductBean product2 = products.get(1);
        ProductBean product3 = products.get(2);

        OrderBean order = buildOrder(1);
        order.setCustomer(customer);
        order.setPositions(new ArrayList<>());
        order.getPositions().add(buildPosition(product1,33));

        // create order
        OrderBean savedOrder = orderService.saveOrder(order);  // create Order
        checkSameOrders(order,savedOrder);
        Assert.assertNotNull(savedOrder.getId());
        Assert.assertEquals(savedOrder.getPositions().size(), 1);
        savedOrder = orderService.findOrderById(savedOrder.getId());
        Assert.assertEquals(savedOrder.getPositions().size(), 1);
        checkSameOrders(order,savedOrder);

        List<OrderBean> newOrders = orderService.findAllOrders();
        Assert.assertEquals(oldOrders.size()+1, newOrders.size());
        List<OrderPositionBean> newPositions = orderService.findAllOrderPositions();
        Assert.assertEquals(oldPositions.size()+1, newPositions.size());


        // updateAttachedEntity order
        order = savedOrder;
        OrderPositionBean position = order.getPositions().get(0);
        position.setProductPrice(567);
        position.setProductAmount(569);

        order.getPositions().add(buildPosition(product2,44));

        order.setDeliveryAddress("rrrrrttttt");
        order.setManager(manager);
        savedOrder = orderService.saveOrder(order); // updateAttachedEntity Order
        Assert.assertEquals(savedOrder .getPositions().size(), 2);
        Assert.assertEquals(order.getManager().getId(), savedOrder.getManager().getId());
        checkSameOrders(order,savedOrder);

        savedOrder = orderService.findOrderById(savedOrder.getId());
        Assert.assertEquals(savedOrder .getPositions().size(), 2);
        Assert.assertEquals(order.getManager().getId(), savedOrder.getManager().getId());
        checkSameOrders(order,savedOrder);


        order = savedOrder;
        order .getPositions().clear();
        order .getPositions().add(buildPosition(product3,66));
        savedOrder = orderService.saveOrder(order); // updateAttachedEntity Order
        Assert.assertEquals(order.getPositions().size(), 1);

        savedOrder = orderService.findOrderById(savedOrder.getId());
        Assert.assertEquals(savedOrder.getPositions().size(), 1);
        OrderPositionBean position3 = savedOrder.getPositions().get(0);
        Assert.assertEquals(position3.getProduct().getId(), product3.getId());

        newPositions = orderService.findAllOrderPositions();
        Assert.assertEquals(oldPositions.size()+1, newPositions.size());

        // delete Order
        orderService.removeOrder(savedOrder.getId());

        List<ProductBean> newProducts = orderService.findAllProducts();
        Assert.assertEquals(newProducts.size(), products.size());


        newOrders = orderService.findAllOrders();
        Assert.assertEquals(oldOrders.size(), newOrders.size());

        newPositions = orderService.findAllOrderPositions();
        Assert.assertEquals(oldPositions.size(), newPositions.size());

    }
    @Test
    public void checkUpdateOrderFields() {

        List<ProductBean> products = orderService.findAllProducts();
        List<UserBean> users = adminService.findAllWithRole(RoleEnum.CUSTOMER.getId());
        List<UserBean> managers = adminService.findAllWithRole(RoleEnum.MANAGER.getId());
        Assert.assertTrue(users.size() > 0);
        Assert.assertTrue(managers.size() > 0);
        UserBean customer = users.get(0);

        Assert.assertTrue(products.size() >= 3);
        ProductBean product1 = products.get(0);
        ProductBean product2 = products.get(1);
        ProductBean product3 = products.get(2);

        OrderBean order = buildOrder(1);
        order.setCustomer(customer);
        order.setPositions(new ArrayList<>());
        order.getPositions().add(buildPosition(product1,33));

        // create order
        OrderBean savedOrder = orderService.saveOrder(order);  // create Order
        Assert.assertNotNull(savedOrder.getId());
        Assert.assertEquals(savedOrder.getPositions().size(), 1);
        savedOrder = orderService.findOrderById(savedOrder.getId());
        Assert.assertEquals(savedOrder.getPositions().size(), 1);

    }
    private OrderPositionBean buildPosition(ProductBean product, int i) {
        OrderPositionBean position = new OrderPositionBean();
        position.setProduct(product);
        position.setProductAmount(i+1);
        position.setProductPrice(i+10);
        return position;

    }
    private OrderBean buildOrder(int i) {
        OrderBean order = new OrderBean();
        order.setDeliveryAddress("Delivery address " + i);
        order.setSubject("Subject " + i);
        order.setDescription("Description " + i);
        return order;
    }
    private void checkSameOrders(OrderBean o1,OrderBean o2) {
        if (o1.getId()!= null && o2.getId()!= null) {
            Assert.assertEquals(o1.getId(), o2.getId());
        }
        Assert.assertEquals(o1.getDescription(),o2.getDescription());
        Assert.assertEquals(o1.getDeliveryAddress(),o2.getDeliveryAddress());
        Assert.assertEquals(o1.getCustomer().getId(),o2.getCustomer().getId());

        if (o1.getManager() == null) {
            Assert.assertNull(o2.getManager());
        } else {
            Assert.assertEquals(o1.getManager().getId(),o2.getManager().getId());
        }
        Assert.assertEquals(o1.getPositions().size(),o2.getPositions().size());
        for (OrderPositionBean p1 : o1.getPositions()) {
            OrderPositionBean p2 = o2.getPositions().stream().filter(it -> p1.getProduct().getId().equals(it.getProduct().getId())).findFirst().get();
            checkSamePositions(p1,p2);

        }
    }
    private void checkSamePositions(OrderPositionBean p1,OrderPositionBean  p2) {
        Assert.assertEquals(p1.getProduct().getId(),p2.getProduct().getId());
        Assert.assertEquals(p1.getProductAmount(),p2.getProductAmount());
        Assert.assertEquals(p1.getProductPrice(),p2.getProductPrice());
    }


    }
