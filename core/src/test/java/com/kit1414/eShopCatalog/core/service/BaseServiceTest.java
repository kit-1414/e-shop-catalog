package com.kit1414.eShopCatalog.core.service;

import javax.sql.DataSource;

import org.dbunit.database.DatabaseDataSourceConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;

import com.kit1414.eShopCatalog.core.HibernateTestConfiguration;

/**
 * Created by kit_1414 on 01-Dec-16.
 */

@ContextConfiguration(classes = { HibernateTestConfiguration.class })
public class BaseServiceTest  extends AbstractTransactionalTestNGSpringContextTests {

    private static final String DATA_FILE_NAME = "test_data.xml";

    @Autowired
    DataSource dataSource;


    protected IDataSet getDataSet() throws Exception {
      IDataSet[] dataSets = new IDataSet[] {
              new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream(DATA_FILE_NAME)),
      };
      return new CompositeDataSet(dataSets );
    }

    @BeforeMethod
    public void setUp() throws Exception {
        IDatabaseConnection dbConn = new DatabaseDataSourceConnection(dataSource);
        DatabaseOperation.CLEAN_INSERT.execute(dbConn, getDataSet());
    }
}
