package com.kit1414.eShopCatalog.core.service;

import com.kit1414.eShopCatalog.core.beans.ProductBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by kit_1414 on 07-Dec-16.
 */
public class ProductsTest extends BaseServiceTest {
    private static final Logger log = LoggerFactory.getLogger(ProductsTest.class);

    @Autowired
    AdminService adminService;

    @Autowired
    OrderService orderService;

    @Test
    public void findAllProducts() {
        List<ProductBean> products = orderService.findAllProducts();
        log.info(String.format(" %d roles founded", products.size()));
        products.forEach(it -> log.info(it.toString()));
        Assert.assertEquals(5,products.size());
    }

    @Test
    public void findProductByIdTest() {
        ProductBean product = orderService.findProductById(1);
        log.info("Product founded :" + product);
        Assert.assertNotNull(product);
    }

    @Test
    public void productCrudTest() {
        List<ProductBean> oldProducts = orderService.findAllProducts();
        Assert.assertEquals(oldProducts.size(),5);

        ProductBean newProduct = buildNewProduct(1);
        log.info("To save:" + newProduct);

        ProductBean saved  = orderService.saveProduct(newProduct);
        log.info("Saved:" + saved);
        Assert.assertNotNull(saved.getId());

        ProductBean searchProduct = orderService.findProductById(saved.getId());
        Assert.assertNotNull(searchProduct);

        checkProductSame(searchProduct,saved);

        List<ProductBean> newProducts = orderService.findAllProducts();
        Assert.assertEquals(newProducts.size(), oldProducts.size() + 1);

        saved.setDescription("ssssss");
        orderService.saveProduct(saved);

        searchProduct = orderService.findProductById(saved.getId());
        Assert.assertNotNull(searchProduct);
        checkProductSame(searchProduct,saved);

        Assert.assertTrue(orderService.removeProduct(searchProduct.getId()));
        Assert.assertFalse(orderService.removeProduct(searchProduct.getId()));

        newProducts = orderService.findAllProducts();
        Assert.assertEquals(newProducts.size(), oldProducts.size());

    }
    private void checkProductSame(ProductBean p1, ProductBean p2) {
        Assert.assertEquals(p1.getId(),p2.getId());
        Assert.assertEquals(p1.getDescription(),p2.getDescription());
        Assert.assertEquals(p1.getProductCode(),p2.getProductCode());
        Assert.assertEquals(p1.getProductName(),p2.getProductName());
    }
    private static ProductBean buildNewProduct(int i) {
        ProductBean newProduct = new ProductBean();
        newProduct.setProductName("New product " + i);
        newProduct.setProductCode("newCode-" + i);
        newProduct.setDescription("new Product description" + i);
        return newProduct;
    }
}
