package com.kit1414.eShopCatalog.core.service;

import com.kit1414.eShopCatalog.core.beans.RoleBean;
import com.kit1414.eShopCatalog.core.enums.RoleEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by kit_1414 on 16-Dec-16.
 */
public class RolesTest  extends BaseServiceTest {
    private static final Logger log = LoggerFactory.getLogger(RolesTest.class);

    @Autowired
    AdminService adminService;

    @Test
    public void testFindAllRolesTest() {
        List<RoleBean> roles = adminService.findAllRoles();
        log.info(String.format(" %d roles founded", roles.size()));
        roles.forEach(it -> log.info(it.toString()));
        Assert.assertEquals(RoleEnum.values().length, roles.size());
    }

}
