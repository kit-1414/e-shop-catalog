-- for all users passwords are 'qwerty'
insert into eshop_role (id, role_name) values (10, 'ADMIN'), (20,'MANAGER'), (30,'CUSTOMER'), (40,'GUEST');

insert into eshop_user (id, login, email, first_name, last_name, pass_md5, description) values (1, 'superuser', 'superuser-email@gmail.com', 'superuserFN', 'superuserLn', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'it''a superuser.'), (2, 'admin', 'admin-email@gmail.com', 'John', 'Smith', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'it''a john Smith. Hi is admin.'), (20, 'manager1', 'manager1@gmail.com', 'manager1FN', 'manager1LN', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'it''a  manager1'), (21, 'manager2', 'manager2@gmail.com', 'manager2FN', 'manager2LN', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'it''a  manager2'), (30, 'customer1', 'customer1@gmail.com', 'customer1FN', 'customer1LN', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'it''a  customer1'), (31, 'customer2', 'customer2@gmail.com', 'customer2FN', 'customer2LN', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'it''a  customer2'), (41, 'guest', 'guest@gmail.com', 'guestFN', 'guestLN', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'it''a  guest');

insert into eshop_user_role  (user_id, role_id) values   (1,10), (1,20),(1,30),(1,40),(2,10), (20,20), (21,20),  (30,30),   (31,30) , (41,40);

insert into eshop_product (id, product_code, product_name, description) values (1, 'code-x1', 'Big blue ball' ,  'Big blue ball description'), (2, 'code-x2', 'Red small ball' , 'Red small ball description'), (3, 'code-x3', 'Kitchen table' ,  'Kitchen table ball description'), (4, 'code-x4', 'Kitchen chair' ,  'Kitchen chair ball description'),  (5, 'code-x5', 'Desk lamp' ,  'Desk lamp description');

insert into eshop_order (id, manager_id, customer_id, description, subject, delivery_address) values  (1, 20, 30,'order 1 description', 'order 1 subject', 'order 1 address'),    (2, 21, 31,'order 2 description', 'order 2 subject', 'order 2 address'), (3, null, 30,'order 3 description', 'order 3 subject', 'order 3 address');

insert into eshop_order_position (id, order_id, product_id, product_amount, product_price) values (1,1,2,2,30), (2,1,3,1,120), (3,2,1,2,37), (4,2,4,3,128), (5,3,2,1,137), (6,3,5,6,228);
