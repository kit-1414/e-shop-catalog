package com.kit1414.eShopCatalog.server.config;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by kit_1414 on 09-Feb-17.
 */
public interface DbHibernateAdapter {
    DataSource buildDataSource();
    Properties buildProperties();
}
