package com.kit1414.eShopCatalog.server.config;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * Created by kit_1414 on 23-Dec-16.
 */
@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan({
        "com.kit1414.eShopCatalog.core.dao.impl",
        "com.kit1414.eShopCatalog.core.service.impl",
        "com.kit1414.eShopCatalog.server.controllers"
})
public class EShopCatalogConfiguration {

    @Autowired
    private Environment environment;

    private final DbHibernateAdapter dbHibernateAdapter = new DbHibernateAdapterH2Impl();

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("com.kit1414.eShopCatalog.core.entity");
        sessionFactory.setHibernateProperties(dbHibernateAdapter.buildProperties());
        return sessionFactory;
    }

    @Bean(name = "dataSource")
    public DataSource dataSource() {
        return dbHibernateAdapter.buildDataSource();
    }

    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory s) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(s);
        return txManager;
    }

}
