package com.kit1414.eShopCatalog.server.config;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by kit_1414 on 09-Feb-17.
 */
public class DbHibernateAdapterH2Impl implements DbHibernateAdapter {

    private static final String DATA_FILE_NAME = "import_data.sql";

    @Override
    public DataSource buildDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE");
        dataSource.setUsername("sa");
        dataSource.setPassword("");


        return dataSource;
    }

    @Override
    public Properties buildProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.hbm2ddl.auto", "create-drop");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.import_files", DATA_FILE_NAME);

        return properties;
    }

}
