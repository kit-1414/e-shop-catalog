package com.kit1414.eShopCatalog.server.controllers;

import com.kit1414.eShopCatalog.core.beans.UserBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by kit_1414 on 14-Feb-17.
 */
@RestController()
public class AdminController extends BaseRestController {
    private static final Logger logger = LoggerFactory.getLogger(com.kit1414.eShopCatalog.server.controllers.AdminController.class);

    //-- Retrieve All Orders ----------------------------------------------
    @RequestMapping(value = "/login", method = RequestMethod.PUT)
    public ResponseEntity<UserBean> login(@RequestParam(value = "login",required = true) final String login, @RequestParam("md5") final String md5) {
        logger.debug("login() login=" + login + ", md5pass=" + md5);
        return doRequest(() -> {
            UserBean user = adminService.login(login, md5);
            return buildUpdateResponse(user);
        });
    }
    @RequestMapping(value = "/logout", method = RequestMethod.PUT)
    public ResponseEntity<String> logout(@RequestParam(value = "login",required = true) final String login) {
        logger.debug("logout() login=" + login);
        return doRequest(() -> {
            // do nothing
            return buildUpdateResponse(login);
        });
    }
    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public ResponseEntity<String> logout() {
        logger.debug("about() ");
        return doRequest(() -> {
            // do nothing
            return buildUpdateResponse("E Shop-Catalog REST API 1.0 ");
        });
    }
}
