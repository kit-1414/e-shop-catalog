package com.kit1414.eShopCatalog.server.controllers;

import com.kit1414.eShopCatalog.core.exceptions.DaoException;
import com.kit1414.eShopCatalog.core.exceptions.ElementNotFoundException;
import com.kit1414.eShopCatalog.core.exceptions.NoContentFoundException;
import com.kit1414.eShopCatalog.core.service.AdminService;
import com.kit1414.eShopCatalog.core.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collection;

/**
 * Created by kit_1414 on 11-Feb-17.
 */
public abstract class BaseRestController {

    private static final Logger logger = LoggerFactory.getLogger(BaseRestController.class);

    protected static final HttpStatus DELETE_RET_CODE = HttpStatus.OK; // HttpStatus.NO_CONTENT ?
    protected static final HttpStatus CREATE_RET_CODE = HttpStatus.OK; //
    protected static final HttpStatus UPDATE_RET_CODE = HttpStatus.OK;
    protected static final HttpStatus GET_RET_CODE = HttpStatus.OK;

    @Autowired
    AdminService adminService;

    @Autowired
    OrderService orderService;


    //---------------------------------------------------------------------------------------------
    // This method catch All exceptions and return right errors
    protected <T> ResponseEntity<T> doRequest(IProcessRequest<T> request) {
        try {
            ResponseEntity<T> response = request.doProcess();
            if (response.getStatusCode() == HttpStatus.OK) {
                T body = response.getBody();
                if (body == null) {
                    throw new ElementNotFoundException();
                }
                if (body instanceof Collection) {
                    Collection c = (Collection) body;
                    if (c.size() == 0) {
                        throw new NoContentFoundException();
                    }
                }
            }
            return response;
        } catch (DaoException ex) {
            logger.info("DaoException happen :" + ex);
            return new ResponseEntity<T>(ex.getHttpStatus());
        } catch (Exception ex) {
            logger.error("Unexpected exception "+ex, ex);
            return new ResponseEntity<T>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    protected interface  IProcessRequest<T> {
        ResponseEntity<T> doProcess();
    }
    protected ResponseEntity<Long> buildCreateResponse(Long id, HttpHeaders headers) {
        return new ResponseEntity<>(id, CREATE_RET_CODE);
    }
    protected <T> ResponseEntity<T> buildUpdateResponse(T body) {
        return new ResponseEntity<T>(body, UPDATE_RET_CODE);
    }
    protected <T> ResponseEntity<T> buildGetResponse(T body) {
        return new ResponseEntity<T>(body, GET_RET_CODE);

    }
    protected ResponseEntity<Boolean> buildDeleteResponse(Boolean deleted) {
        return new ResponseEntity<>(deleted, deleted? DELETE_RET_CODE : HttpStatus.NOT_FOUND);
    }
}
