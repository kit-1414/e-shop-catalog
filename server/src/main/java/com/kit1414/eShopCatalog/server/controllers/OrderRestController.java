package com.kit1414.eShopCatalog.server.controllers;

import com.kit1414.eShopCatalog.core.beans.OrderBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by kit_1414 on 11-Feb-17.
 */

@RestController()
public class OrderRestController extends BaseRestController {
    private static final Logger logger = LoggerFactory.getLogger(OrderRestController.class);

    //-- Retrieve All Orders ----------------------------------------------
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public ResponseEntity<List<OrderBean>> listAllOrders() {
        logger.debug("listAllOrders()");
        return doRequest(() -> {
            List<OrderBean> orders = orderService.findAllOrders();
            return buildGetResponse(orders);
        });
    }

    //------------------- Retrieve Single Order -----------------------------------
    @RequestMapping(value = "/order/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderBean> getOrder(@PathVariable("id") long id) {
        logger.debug("getOrder() id=" + id);
        return doRequest(() -> {
            OrderBean order = orderService.findOrderById(id);
            return buildGetResponse(order);
        });
    }

    //------------------- Create a Order --------------------------------------------------------
    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public ResponseEntity<Long> createOrder(@RequestBody OrderBean order, UriComponentsBuilder ucBuilder) {
        logger.debug("createOrder() order=" + order);
        return doRequest(() -> {
            order.setId(null); // TODO ?
            OrderBean newOrder = orderService.saveOrder(order);

            logger.debug("createOrder() OK, order=" + newOrder);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/order/{id}").buildAndExpand(newOrder.getId()).toUri());
            return buildCreateResponse(newOrder.getId(), headers);
        });
    }


    //------------------- Update a Order  --------------------------------------------------------
    @RequestMapping(value = "/order/{id}", method = RequestMethod.PUT)
    public ResponseEntity<OrderBean> updateOrder(@PathVariable("id") long id, @RequestBody OrderBean order) {
        logger.debug("updateOrder() id=" + id + ", order=" + order);
        return doRequest(() -> {
            OrderBean currentOrder = orderService.findOrderById(id);

            if (currentOrder == null) {
                logger.debug("Order with id " + id + " not found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            order.setId(id);
            orderService.saveOrder(order);
            currentOrder = orderService.findOrderById(id);
            return buildUpdateResponse(currentOrder);
        });
    }

    //------------------- Delete a Order --------------------------------------------------------
    @RequestMapping(value = "/order/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> deleteOrder(@PathVariable("id") final long id) {
        logger.debug("deleteOrder() id=" + id);
        return doRequest(() -> {
            Boolean founded = orderService.removeOrder(id);
            return buildDeleteResponse(founded);
        });
    }
}
