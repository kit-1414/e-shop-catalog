package com.kit1414.eShopCatalog.server.controllers;

import com.kit1414.eShopCatalog.core.beans.ProductBean;
import com.kit1414.eShopCatalog.core.beans.UserBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by kit_1414 on 11-Feb-17.
 */

@RestController()
public class ProductRestController extends BaseRestController {

    private static final Logger logger = LoggerFactory.getLogger(ProductRestController.class);

    //-- Retrieve All Products ----------------------------------------------
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public ResponseEntity<List<ProductBean>> listAllProducts() {
        logger.debug("listAllProducts()");
        return doRequest(()-> {
            List<ProductBean> products = orderService.findAllProducts();
            return buildGetResponse(products);
        });
    }

    //------------------- Retrieve Single Product -----------------------------------
    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductBean> getProduct(@PathVariable("id") long id) {
        logger.debug("getProduct() id=" + id);
        return doRequest(()-> {
            ProductBean product = orderService.findProductById(id);
            return buildGetResponse(product);
        });
    }

    //------------------- Create a Product --------------------------------------------------------
    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public ResponseEntity<Long> createProduct(@RequestBody ProductBean product, UriComponentsBuilder ucBuilder) {
        logger.debug("createProduct() product=" + product);
        return doRequest(()-> {

            product.setId(null); // TODO ?
            ProductBean newProduct = orderService.saveProduct(product);

            logger.debug("createProduct() OK newProduct=" + newProduct);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/product/{id}").buildAndExpand(newProduct.getId()).toUri());
            return buildCreateResponse(newProduct.getId(), headers);
        });
    }


    //------------------- Update a Product  --------------------------------------------------------
    @RequestMapping(value = "/product/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ProductBean> updateProduct(@PathVariable("id") long id, @RequestBody ProductBean product) {
        logger.debug("updateProduct() id=" + id + ", product=" + product);
        return doRequest(()-> {

            ProductBean currentProduct = orderService.findProductById(id);

            if (currentProduct == null) {
                logger.debug("Product with id " + id + " not found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            product.setId(id);
            orderService.saveProduct(product);
            currentProduct = orderService.findProductById(id);
            return buildUpdateResponse(currentProduct);
        });
    }

    //------------------- Delete a Product --------------------------------------------------------
    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> deleteProduct(@PathVariable("id") final long id) {
        logger.debug("deleteProduct() id=" + id);
        return doRequest(()-> {
            Boolean founded = orderService.removeProduct(id);
            return buildDeleteResponse(founded);
        });
    }

}
