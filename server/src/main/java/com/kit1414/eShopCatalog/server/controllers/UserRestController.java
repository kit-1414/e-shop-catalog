package  com.kit1414.eShopCatalog.server.controllers;

import com.kit1414.eShopCatalog.core.beans.RoleBean;
import com.kit1414.eShopCatalog.core.beans.UserBean;
import com.kit1414.eShopCatalog.core.exceptions.DaoException;
import com.kit1414.eShopCatalog.core.exceptions.ElementNotFoundException;
import com.kit1414.eShopCatalog.core.exceptions.NoContentFoundException;
import com.kit1414.eShopCatalog.core.service.AdminService;
import com.kit1414.eShopCatalog.core.service.impl.OrderServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController()
public class UserRestController extends BaseRestController {

    private static final Logger logger = LoggerFactory.getLogger(UserRestController .class);

    //-- Retrieve All Roles ----------------------------------------------
    @RequestMapping(value = "/roles", method = RequestMethod.GET)
    public ResponseEntity<List<RoleBean>> listAllRoles() {
        logger.debug("listAllRoles()");
        return doRequest(()-> {
            List<RoleBean> roles = adminService.findAllRoles();
            return buildGetResponse(roles);
        });
    }

    //-- Retrieve All Users ----------------------------------------------
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<List<UserBean>> listAllUsers() {
        logger.debug("listAllUsers()");
        return doRequest(()-> {
            List<UserBean> users = adminService.findAllUsers();
            return buildGetResponse(users);
        });
    }

    //-------------------Retrieve Single User--------------------------------------------------------
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserBean> getUser(@PathVariable("id") long id) {
        logger.debug("getUser() id=" + id);
        return doRequest(()-> {
            UserBean user = adminService.findUserById(id);
            return buildGetResponse(user);
        });
    }

    //-------------------Create a User--------------------------------------------------------
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public ResponseEntity<Long> createUser(@RequestBody UserBean user,    UriComponentsBuilder ucBuilder) {
        logger.debug("createUser() " + user);
        return doRequest(()-> {

            user.setId(null); // TODO ?
            UserBean newUser = adminService.saveUser(user);
            logger.debug("createUser() OK, newUser=" + newUser);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(newUser.getId()).toUri());
            return buildCreateResponse(newUser.getId(), headers);
        });
    }


    //------------------- Update a User --------------------------------------------------------

    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UserBean> updateUser(@PathVariable("id") long id, @RequestBody UserBean user) {
        logger.debug("updateUser() id=" +id+ ", user="+ user);
        return doRequest(()-> {
            UserBean currentUser = adminService.findUserById(id);

            if (currentUser == null) {
                logger.debug("User with id " + id + " not found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            user.setId(id);
            adminService.saveUser(user);
            currentUser = adminService.findUserById(id);
            return buildUpdateResponse(currentUser);
        });
    }

    //------------------- Delete a User --------------------------------------------------------
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> deleteUser(@PathVariable("id") final long id) {
        logger.debug("deleteUser() id=" + id);
        return doRequest(()-> {
                Boolean founded = adminService.removeUser(id);
                return buildDeleteResponse(founded);
        });
    }
}